package com.jobsearchgatewayservice.gatewayapp.configs;

import com.jobsearchgatewayservice.gatewayapp.filters.AdminFilter;
import com.jobsearchgatewayservice.gatewayapp.filters.AppFilter;
import com.jobsearchgatewayservice.gatewayapp.filters.UserFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GatewayConfig {
    private final AppFilter appFilter;
    private final AdminFilter adminFilter;
    private final UserFilter userFilter;

    @Value("${auth.uri}")
    private String authUri;

    @Value("${user.uri}")
    private String userUri;

    @Value("${jobs.uri}")
    private String jobsUri;

    @Bean
    RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/api/v1/user-application/**")
                        .filters(f -> f.filter(appFilter).filter(userFilter))
                        .uri(jobsUri)
                )
                .route(r -> r.path("/api/v1/jobs/applications/**")
                        .filters(f -> f.filter(appFilter).filter(adminFilter))
                        .uri(jobsUri)
                )
                .route(r -> r.path("/api/v1/jobs/**")
                        .and().method("POST", "PUT", "DELETE")
                        .filters(f -> f.filter(appFilter).filter(adminFilter))
                        .uri(jobsUri)
                )
                .route(r -> r.path("/api/v1/jobs/**")
                        .and().method("GET")
                        .uri(jobsUri)
                )
                .route(r -> r.path("/api/v1/profile/**")
                        .filters(f -> f.filter(appFilter))
                        .uri(userUri))
                .route(r -> r.path("/api/v1/user/**")
                        .filters(f -> f.filter(appFilter).filter(adminFilter))
                        .uri(userUri)
                )
                .route(r -> r.path("/api/v1/auth/**").uri(authUri))
                .build();
    }
}
