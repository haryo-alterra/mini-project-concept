package com.jobsearchgatewayservice.gatewayapp.filters;

import com.jobsearchgatewayservice.gatewayapp.dtos.ResponseDTO;
import com.jobsearchgatewayservice.gatewayapp.dtos.UserDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@Log4j2
@Component
@RequiredArgsConstructor
public class AppFilter implements GatewayFilter {
    @Value("${auth.uri}")
    private String authUri;

    private final RestTemplate restTemplate;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        final List<String> apiEndpoints = List.of("/api/v1/auth/register", "/api/v1/auth/login");

        Predicate<ServerHttpRequest> isApiSecured = r -> apiEndpoints.stream()
                .noneMatch(uri -> r.getURI().getPath().contains(uri));
        if (isApiSecured.test(request)) {
            if (!request.getHeaders().containsKey("Authorization")) {
                ServerHttpResponse response = exchange.getResponse();
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
            final String token = request.getHeaders().getOrEmpty("Authorization").get(0);
            String newToken = token.split(" ")[1];
            HttpHeaders headers = new HttpHeaders();
            headers.setBearerAuth(newToken);

            HttpEntity<String> entity = new HttpEntity<>(headers);
            ResponseEntity<ResponseDTO<UserDTO>> response = restTemplate.exchange(
                    authUri+"/api/v1/auth/user_data",
                    HttpMethod.GET, entity, new ParameterizedTypeReference<>() {});
            exchange.getRequest().mutate().header(
                    "role_id", String.valueOf(
                            Objects.requireNonNull(response.getBody().getData().getRoleId()))).build();
            exchange.getRequest().mutate().header(
                    "user_id", Objects.requireNonNull(
                            response.getBody().getData().getId().toString())).build();
            exchange.getRequest().mutate().header(
                    "name", String.valueOf(
                            Objects.requireNonNull(response.getBody().getData().getName()))).build();
        }
        return chain.filter(exchange);
    }
}
