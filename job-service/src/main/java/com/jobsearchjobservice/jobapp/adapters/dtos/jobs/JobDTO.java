package com.jobsearchjobservice.jobapp.adapters.dtos.jobs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class JobDTO implements Serializable {
    private Long id;

    @JsonProperty(required = true)
    private String title;
    @JsonProperty(required = true)
    private String description;

    @JsonProperty(value = "salary_low")
    private int salaryLow;

    @JsonProperty(value = "salary_high")
    private int salaryHigh;

    // Insert it in yyyy-MM-dd format
    @JsonProperty(required = true, value = "expiry_date")
    private String expiryDate;

    @JsonProperty(value = "applicant_counts")
    private int applicantCounts;
}
