package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPostUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobPostController {
    private final JobPostUseCase jobPostUseCase;

    @PostMapping
    public ResponseEntity<JobResponseDTO> postNewJob(@RequestBody JobDTO job) {
        JobDTO resultingJob = jobPostUseCase.postNewJob(job);
        return new ResponseEntity<>(JobResponseDTO.builder()
                .httpStatus(HttpStatus.CREATED)
                .message("Job successfully created")
                .data(resultingJob).build(), HttpStatus.CREATED
        );
    }
}
