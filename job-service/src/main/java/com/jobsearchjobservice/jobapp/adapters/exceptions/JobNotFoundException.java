package com.jobsearchjobservice.jobapp.adapters.exceptions;

import com.jobsearchjobservice.jobapp.adapters.exceptions.commons.DataNotExistException;

public class JobNotFoundException extends DataNotExistException {
    public JobNotFoundException() {
        super("Job with given id not found");
    }
}
