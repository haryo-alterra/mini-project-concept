package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobGetAllPaginationUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobGetAllPaginationController {
    private final JobGetAllPaginationUseCase jobGetAllPaginationUseCase;

    @GetMapping
    public ResponseEntity<PaginationJobsResponseDTO> getAllJobs(
            @PageableDefault Pageable pageable) {
        PaginationJobsDTO listOfJobs = jobGetAllPaginationUseCase.getAllJobsPagination(pageable);
        return new ResponseEntity<>(PaginationJobsResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Retrieved all jobs posting")
                .data(listOfJobs).build(), HttpStatus.OK
        );
    }
}
