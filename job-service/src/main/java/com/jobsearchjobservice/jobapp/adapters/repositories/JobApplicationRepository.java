package com.jobsearchjobservice.jobapp.adapters.repositories;

import com.jobsearchjobservice.jobapp.adapters.entities.JobApplicationEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface JobApplicationRepository extends JpaRepository<JobApplicationEntity, Long> {
    @Query(value = "select * from job_applications ja where ja.job_id = ?1 and ja.user_id = ?2",
            nativeQuery = true)
    Optional<JobApplicationEntity> findByJobAndUserId(Long jobId, UUID userId);

    @Modifying
    @Query(value = "insert into job_applications (" +
            "user_id, applicant_name, job_id, message, expected_salary, expected_join_date, submitted_at) " +
            "values (?1, ?2, ?3, ?4, ?5, ?6, now())",
            nativeQuery = true)
    void saveNewJobApplication(UUID userId, String applicantName, Long jobId,
                               String message, int expectedSalary, Date expectedJoinDate);

    Page<JobApplicationEntity> findAllByUserId(UUID userId, Pageable pageable);

    Page<JobApplicationEntity> findAllByJobId(Long jobId, Pageable pageable);

    @Modifying
    @Query(value = "update job_applications set applicant_name = ?2 where user_id = ?1",
            nativeQuery = true)
    void updateNameInJobApplication(UUID userId, String newName);
}
