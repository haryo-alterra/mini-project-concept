package com.jobsearchjobservice.jobapp.adapters.persistences;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.*;
import com.jobsearchjobservice.jobapp.adapters.entities.JobApplicationEntity;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.*;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobApplicationRepository;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationAllByJobPort;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationAllByUserPort;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationDeletePort;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationSubmitPort;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobUserNameChangePort;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"job_applications"})
public class JobApplicationPersistence implements
        JobApplicationSubmitPort,
        JobApplicationAllByUserPort,
        JobApplicationDeletePort,
        JobApplicationAllByJobPort,
        JobUserNameChangePort {
    private final JobRepository jobRepository;
    private final JobApplicationRepository jobAppRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    @CacheEvict(value = {"jobs", "job_applications"}, allEntries = true)
    public JobApplicationDTO submitApplication(
            String userId, String fullName, JobApplicationDTO app) {
        UUID uuidUser = UUID.fromString(userId);
        Date today = Date.valueOf(LocalDate.now());
        Date expiryDate = Date.valueOf(app.getExpectedJoinDate());

        if (app.getExpectedSalary() < 0) {
            throw new InvalidSalaryRangeException("Salary can't be a negative number");
        }
        if (expiryDate.before(today)) {
            throw new InvalidDateException("Can't set join date in the past");
        }

        Optional<JobEntity> referredJob = jobRepository.findById(app.getJobId());
        if (referredJob.isEmpty()) {
            throw new JobNotFoundException();
        }

        Optional<JobApplicationEntity> foundApplication
                = jobAppRepository.findByJobAndUserId(app.getJobId(), uuidUser);
        if (foundApplication.isPresent()) {
            throw new JobApplicationExistsException();
        }
        jobAppRepository.saveNewJobApplication(
                uuidUser, fullName, app.getJobId(), app.getMessage(),
                app.getExpectedSalary(), Date.valueOf(app.getExpectedJoinDate()));

        JobEntity updatedCount = referredJob.get();
        updatedCount.setApplicantCounts(updatedCount.getApplicantCounts() + 1);
        jobRepository.save(updatedCount);

        return app;
    }

    @Override
    @Cacheable
    public PaginationJobAppEntityDTO getAllApplicationByUser(String userId, Pageable pageable) {
        UUID uuidUser = UUID.fromString(userId);
        List<JobApplicationEntityDTO> jobDTOList = new ArrayList<>();
        Page<JobApplicationEntity> allUserApplications
                = jobAppRepository.findAllByUserId(uuidUser, pageable);
        for (JobApplicationEntity entity: allUserApplications.getContent()) {
            jobDTOList.add(modelMapper.map(entity, JobApplicationEntityDTO.class));
        }
        return PaginationJobAppEntityDTO.builder()
                .jobAppDTOList(jobDTOList)
                .totalOfItems(allUserApplications.getTotalElements())
                .totalOfPages(allUserApplications.getTotalPages())
                .currentPage(allUserApplications.getNumber())
                .build();
    }

    @Override
    @Transactional
    @CacheEvict(value = {"jobs", "job_applications"}, allEntries = true)
    public JobApplicationEntityDTO deleteApplication(String userId, Long jobId) {
        UUID uuidUser = UUID.fromString(userId);
        Optional<JobApplicationEntity> foundApplication
                = jobAppRepository.findByJobAndUserId(jobId, uuidUser);
        if (foundApplication.isEmpty()) {
            throw new JobApplicationNotFoundException();
        }
        JobApplicationEntity deletedUserApplication = foundApplication.get();
        jobAppRepository.delete(deletedUserApplication);

        Optional<JobEntity> referredJob = jobRepository.findById(jobId);
        JobEntity updatedCount = referredJob.get();
        updatedCount.setApplicantCounts(updatedCount.getApplicantCounts() - 1);
        jobRepository.save(updatedCount);

        return modelMapper.map(deletedUserApplication, JobApplicationEntityDTO.class);
    }

    @Override
    public PaginationJobAppEntityAdminDTO getAllApplicationByJob(Long jobId, Pageable pageable) {
        Optional<JobEntity> job = jobRepository.findById(jobId);
        if (job.isEmpty()) {
            throw new JobNotFoundException();
        }
        List<JobApplicationEntityAdminDTO> jobDTOList = new ArrayList<>();
        Page<JobApplicationEntity> allApplicationsInJob
                = jobAppRepository.findAllByJobId(jobId, pageable);
        for (JobApplicationEntity entity: allApplicationsInJob.getContent()) {
            jobDTOList.add(modelMapper.map(entity, JobApplicationEntityAdminDTO.class));
        }
        return PaginationJobAppEntityAdminDTO.builder()
                .jobAppDTOList(jobDTOList)
                .totalOfItems(allApplicationsInJob.getTotalElements())
                .totalOfPages(allApplicationsInJob.getTotalPages())
                .currentPage(allApplicationsInJob.getNumber())
                .build();
    }

    @Override
    @CacheEvict(value = {"job_applications"}, allEntries = true)
    @Transactional
    public void autoUpdateName(String userId, String newName) {
        UUID uuidUser = UUID.fromString(userId);
        jobAppRepository.updateNameInJobApplication(uuidUser, newName);
    }
}
