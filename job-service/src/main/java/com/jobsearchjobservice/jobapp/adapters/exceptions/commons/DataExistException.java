package com.jobsearchjobservice.jobapp.adapters.exceptions.commons;

public class DataExistException extends RuntimeException {
    public DataExistException() {
        super();
    }

    public DataExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataExistException(String message) {
        super(message);
    }

    public DataExistException(Throwable cause) {
        super(cause);
    }
}
