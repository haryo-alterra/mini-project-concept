package com.jobsearchjobservice.jobapp.adapters.repositories;

import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, Long> {
    Page<JobEntity> findAll(Pageable pageable);
    Optional<JobEntity> findById(Long id);
}
