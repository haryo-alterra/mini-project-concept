package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityAdminDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityAdminResDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationAllByJobUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobApplicationAllByJobController {
    private final JobApplicationAllByJobUseCase jobAppByJobUseCase;

    @GetMapping("/applications/{jobId}")
    public ResponseEntity<PaginationJobAppEntityAdminResDTO> getApplicationByJob(
            @PathVariable Long jobId,
            @PageableDefault Pageable pageable) {
        PaginationJobAppEntityAdminDTO applicationList
                = jobAppByJobUseCase.getAllApplicationByJob(jobId, pageable);
        return new ResponseEntity<>(PaginationJobAppEntityAdminResDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved all submissions for this job")
                .data(applicationList).build(), HttpStatus.OK
        );
    }
}
