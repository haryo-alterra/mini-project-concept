package com.jobsearchjobservice.jobapp.adapters.exceptions;

import com.jobsearchjobservice.jobapp.adapters.exceptions.commons.DataExistException;

public class JobApplicationExistsException extends DataExistException {
    public JobApplicationExistsException() {
        super("You already submitted application for this job");
    }
}
