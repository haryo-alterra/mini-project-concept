package com.jobsearchjobservice.jobapp.adapters.exceptions;

import com.jobsearchjobservice.jobapp.adapters.dtos.commons.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandlerAdvice {
    @ExceptionHandler(value = InvalidDateException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerInvalidDate(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.BAD_REQUEST)
                .message("Invalid date input")
                .data(e.getMessage())
                .build(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = InvalidSalaryRangeException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerInvalidSalaryRange(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.BAD_REQUEST)
                .message("Invalid salary range input")
                .data(e.getMessage())
                .build(), HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = JobNotFoundException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerJobNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.NOT_FOUND)
                .message("Job not found")
                .data(e.getMessage())
                .build(), HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(value = JobApplicationExistsException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerJobApplicationExists(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.CONFLICT)
                .message("Job application already exists")
                .data(e.getMessage())
                .build(), HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(value = JobApplicationNotFoundException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerJobApplicationNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.NOT_FOUND)
                .message("Referred job application for this account not found.")
                .data(e.getMessage())
                .build(), HttpStatus.NOT_FOUND
        );
    }

}
