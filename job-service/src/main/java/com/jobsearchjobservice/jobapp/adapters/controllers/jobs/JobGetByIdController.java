package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobGetByIdUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobGetByIdController {
    private final JobGetByIdUseCase jobGetByIdUseCase;

    @GetMapping("/{jobId}")
    public ResponseEntity<JobResponseDTO> deleteJob(@PathVariable Long jobId) {
        JobDTO job = jobGetByIdUseCase.getJob(jobId);
        return new ResponseEntity<>(JobResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved job data")
                .data(job).build(), HttpStatus.OK
        );
    }
}
