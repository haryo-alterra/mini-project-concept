package com.jobsearchjobservice.jobapp.adapters.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name="job_applications")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class JobApplicationEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "job_id")
    private JobEntity job;

    @Column(name = "user_id", nullable = false)
    @JsonProperty(value = "user_id")
    private UUID userId;

    @Column(name = "applicant_name", nullable = false)
    @JsonProperty(value = "applicant_name")
    private String applicantName;

    @Column(columnDefinition = "TEXT")
    private String message;

    @Column(columnDefinition = "NUMERIC", nullable = false)
    @JsonProperty(value = "expected_salary")
    private int expectedSalary;

    @Column(name = "expected_join_date", nullable = false)
    private Date expectedJoinDate;

    @CreationTimestamp
    @Column(name = "submitted_at", nullable = false, updatable = false)
    private Date createdAt;
}
