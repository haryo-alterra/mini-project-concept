package com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class PaginationJobAppEntityAdminDTO implements Serializable {
    @JsonProperty(value = "job_applications_list")
    private List<JobApplicationEntityAdminDTO> jobAppDTOList;

    @JsonProperty(value = "total_of_items")
    private Long totalOfItems;

    @JsonProperty(value = "current_page")
    private Integer currentPage;

    @JsonProperty(value = "total_of_pages")
    private Integer totalOfPages;
}
