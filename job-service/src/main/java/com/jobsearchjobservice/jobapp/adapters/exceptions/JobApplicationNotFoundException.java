package com.jobsearchjobservice.jobapp.adapters.exceptions;

import com.jobsearchjobservice.jobapp.adapters.exceptions.commons.DataNotExistException;

public class JobApplicationNotFoundException extends DataNotExistException {
    public JobApplicationNotFoundException() {
        super("Job application for this user id / job id not found");
    }
}
