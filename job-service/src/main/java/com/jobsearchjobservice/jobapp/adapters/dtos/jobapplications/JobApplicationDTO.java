package com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class JobApplicationDTO implements Serializable {
    @JsonProperty(value = "job_id", required = true)
    private Long jobId;

    private String message;

    @JsonProperty(value = "expected_salary", required = true)
    private int expectedSalary;

    @JsonProperty(value = "expected_join_date", required = true)
    private String expectedJoinDate;
}
