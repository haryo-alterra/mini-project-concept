package com.jobsearchjobservice.jobapp.adapters.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name="jobs")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class JobEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String description;

    @Column(columnDefinition = "NUMERIC")
    private int salaryLow = 0;

    @Column(columnDefinition = "NUMERIC")
    private int salaryHigh = 0;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @Column(name = "expired_at", nullable = false)
    private Date expiredAt;

    @Column(name = "applicant_counts")
    private int applicantCounts = 0;

    @JsonIgnore
    @OneToMany(mappedBy = "job", orphanRemoval=true)
    private List<JobApplicationEntity> jobApplications;
}
