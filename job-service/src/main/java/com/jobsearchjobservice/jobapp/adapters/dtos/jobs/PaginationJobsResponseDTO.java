package com.jobsearchjobservice.jobapp.adapters.dtos.jobs;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class PaginationJobsResponseDTO implements Serializable {
    private HttpStatus httpStatus;
    private String message;
    private PaginationJobsDTO data;
}
