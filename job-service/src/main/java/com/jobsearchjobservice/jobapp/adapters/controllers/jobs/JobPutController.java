package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPutUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobPutController {
    private final JobPutUseCase jobPutUseCase;

    @PutMapping("/{jobId}")
    public ResponseEntity<JobResponseDTO> editJob(
            @PathVariable Long jobId,
            @RequestBody JobDTO job) {
        JobDTO editedJob = jobPutUseCase.editJob(jobId, job);
        return new ResponseEntity<>(JobResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Job successfully updated")
                .data(editedJob).build(), HttpStatus.OK
        );
    }
}
