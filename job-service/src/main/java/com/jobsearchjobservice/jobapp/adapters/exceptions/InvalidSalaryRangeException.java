package com.jobsearchjobservice.jobapp.adapters.exceptions;

public class InvalidSalaryRangeException extends RuntimeException {
    public InvalidSalaryRangeException(String message) {
        super(message);
    }
}
