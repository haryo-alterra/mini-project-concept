package com.jobsearchjobservice.jobapp.adapters.persistences;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidDateException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidSalaryRangeException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.application.out.jobs.*;
import com.jobsearchjobservice.jobapp.utils.EntityToDTOJobConverter;
import com.jobsearchjobservice.jobapp.utils.UpdateJobAttribute;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@CacheConfig(cacheNames = {"jobs"})
public class JobPersistence implements
        JobPostPort,
        JobGetAllPaginationPort,
        JobGetByIdPort,
        JobDeletePort,
        JobPutPort {
    private final JobRepository jobRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    @CacheEvict(value = "jobs", allEntries = true)
    public JobDTO postNewJob(JobDTO job) {
        JobEntity newJob = modelMapper.map(job, JobEntity.class);
        Date today = Date.valueOf(LocalDate.now());
        Date expiryDate = Date.valueOf(job.getExpiryDate());
        if (newJob.getSalaryLow() > newJob.getSalaryHigh()) {
            throw new InvalidSalaryRangeException(
                "Lower limit of the salary range can't be higher than the upper limit");
        }
        if (newJob.getSalaryLow() < 0 || newJob.getSalaryHigh() < 0) {
            throw new InvalidSalaryRangeException("Salary can't be a negative number");
        }
        if (expiryDate.before(today)) {
            throw new InvalidDateException("Can't post job with submission deadline in the past");
        }
        newJob.setExpiredAt(expiryDate);
        JobEntity savedJob = jobRepository.save(newJob);
        return EntityToDTOJobConverter.convert(savedJob);
    }

    @Override
    @Cacheable
    public PaginationJobsDTO getAllJobsPagination(Pageable pageable) {
        Page<JobEntity> jobs = jobRepository.findAll(pageable);
        List<JobDTO> jobsDTOList = new ArrayList<>();
        for (JobEntity job : jobs.getContent()) {
            JobDTO jobDTO = EntityToDTOJobConverter.convert(job);
            jobsDTOList.add(jobDTO);
        }
        return PaginationJobsDTO.builder()
                .jobDTOList(jobsDTOList)
                .totalOfItems(jobs.getTotalElements())
                .totalOfPages(jobs.getTotalPages())
                .currentPage(jobs.getNumber())
                .build();
    }

    @Override
    public JobDTO getJob(Long id) {
        Optional<JobEntity> job = jobRepository.findById(id);
        if (job.isEmpty()) {
            throw new JobNotFoundException();
        }
        return EntityToDTOJobConverter.convert(job.get());
    }

    @Override
    @Transactional
    @CacheEvict(value = {"jobs", "job_applications"}, allEntries = true)
    public JobDTO deleteJob(Long id) {
        Optional<JobEntity> job = jobRepository.findById(id);
        if (job.isEmpty()) {
            throw new JobNotFoundException();
        }
        JobEntity deletedJob = job.get();
        jobRepository.delete(deletedJob);
        return EntityToDTOJobConverter.convert(deletedJob);
    }

    @Override
    @Transactional
    @CacheEvict(value = {"jobs", "job_applications"}, allEntries = true)
    public JobDTO editJob(Long id, JobDTO job) {
        Optional<JobEntity> registeredJob = jobRepository.findById(id);
        if (registeredJob.isEmpty()) {
            throw new JobNotFoundException();
        }
        JobEntity jobUpdate = UpdateJobAttribute.updateAttribute(registeredJob.get(), job);
        Date today = Date.valueOf(LocalDate.now());

        if (jobUpdate.getSalaryLow() > jobUpdate.getSalaryHigh()) {
            throw new InvalidSalaryRangeException(
                "Lower limit of the salary range can't be higher than the upper limit");
        }
        if (jobUpdate.getSalaryLow() < 0 || jobUpdate.getSalaryHigh() < 0) {
            throw new InvalidSalaryRangeException("Salary can't be a negative number");
        }
        if (jobUpdate.getExpiredAt().before(today)) {
            throw new InvalidDateException("Can't post job with submission deadline in the past");
        }

        jobUpdate.setId(id);
        JobEntity updatedJob = jobRepository.save(jobUpdate);
        return EntityToDTOJobConverter.convert(updatedJob);
    }
}
