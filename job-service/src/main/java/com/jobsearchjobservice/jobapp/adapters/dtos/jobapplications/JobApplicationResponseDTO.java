package com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class JobApplicationResponseDTO implements Serializable {
    private HttpStatus httpStatus;
    private String message;
    private JobApplicationDTO data;
}
