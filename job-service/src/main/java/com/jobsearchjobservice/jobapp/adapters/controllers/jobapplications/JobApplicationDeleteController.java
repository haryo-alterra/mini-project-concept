package com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationDeleteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user-application")
@RequiredArgsConstructor
public class JobApplicationDeleteController {
    private final JobApplicationDeleteUseCase jobAppDeleteUseCase;

    @DeleteMapping("/{jobId}")
    public ResponseEntity<JobApplicationEntityResponseDTO> deleteApplication(
            @RequestHeader(name = "user_id") String userId,
            @PathVariable Long jobId) {
        JobApplicationEntityDTO deletedJobApp
                = jobAppDeleteUseCase.deleteApplication(userId, jobId);
        return new ResponseEntity<>(JobApplicationEntityResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully deleted your job application")
                .data(deletedJobApp).build(), HttpStatus.OK
        );
    }
}
