package com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationSubmitUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user-application")
@RequiredArgsConstructor
public class JobApplicationSubmitController {
    private final JobApplicationSubmitUseCase jobAppSubmitUseCase;

    @PostMapping
    public ResponseEntity<JobApplicationResponseDTO> submitApplication(
            @RequestHeader(name = "user_id") String userId,
            @RequestHeader(name = "name") String fullName,
            @RequestBody JobApplicationDTO app) {
        JobApplicationDTO newlySubmittedApp = jobAppSubmitUseCase
                .submitApplication(userId, fullName, app);
        return new ResponseEntity<>(JobApplicationResponseDTO.builder()
                .httpStatus(HttpStatus.CREATED)
                .message("Successfully submitted your job application")
                .data(newlySubmittedApp).build(), HttpStatus.CREATED
        );
    }
}
