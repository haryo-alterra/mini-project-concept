package com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications;

import com.jobsearchjobservice.jobapp.application.in.jobs.JobUserNameChangeUseCase;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequiredArgsConstructor
public class JobUserNameChangeController {
    private final JobUserNameChangeUseCase jobUserNameChangeUseCase;

    @KafkaListener(
            topics = "${tpc.topic-name}",
            clientIdPrefix = "json",
            containerFactory = "kafkaListenerContainerFactory"
    )
    public void autoUpdateName(
            ConsumerRecord<String, HashMap> cr,
            @Payload HashMap payload) {
        String userId = (String) payload.get("user_id");
        String name = (String) payload.get("name");
        jobUserNameChangeUseCase.autoUpdateName(userId, name);
    }
}
