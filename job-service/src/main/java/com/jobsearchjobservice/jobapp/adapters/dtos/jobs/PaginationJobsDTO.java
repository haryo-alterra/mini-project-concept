package com.jobsearchjobservice.jobapp.adapters.dtos.jobs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class PaginationJobsDTO implements Serializable {
    @JsonProperty(value = "jobs_list")
    private List<JobDTO> jobDTOList;

    @JsonProperty(value = "total_of_items")
    private Long totalOfItems;

    @JsonProperty(value = "current_page")
    private Integer currentPage;

    @JsonProperty(value = "total_of_pages")
    private Integer totalOfPages;
}
