package com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationAllByUserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user-application")
@RequiredArgsConstructor
public class JobApplicationAllByUserController {
    private final JobApplicationAllByUserUseCase jobAppByUserUseCase;

    @GetMapping
    public ResponseEntity<PaginationJobAppEntityResponseDTO> getAllApplicationByUser (
            @RequestHeader(name = "user_id") String userId,
            @PageableDefault Pageable pageable) {
        PaginationJobAppEntityDTO jobList = jobAppByUserUseCase
                .getAllApplicationByUser(userId, pageable);
        return new ResponseEntity<>(PaginationJobAppEntityResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved all your job submissions")
                .data(jobList).build(), HttpStatus.OK
        );
    }
}
