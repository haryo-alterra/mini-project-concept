package com.jobsearchjobservice.jobapp.adapters.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobResponseDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobDeleteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/jobs")
@RequiredArgsConstructor
public class JobDeleteController {
    private final JobDeleteUseCase jobDeleteUseCase;

    @DeleteMapping("/{jobId}")
    public ResponseEntity<JobResponseDTO> deleteJob(@PathVariable Long jobId) {
        JobDTO deletedJob = jobDeleteUseCase.deleteJob(jobId);
        return new ResponseEntity<>(JobResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Job successfully deleted")
                .data(deletedJob).build(), HttpStatus.OK
        );
    }
}
