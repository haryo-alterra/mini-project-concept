package com.jobsearchjobservice.jobapp.adapters.exceptions.commons;

public class DataNotExistException extends RuntimeException {
    public DataNotExistException() {
        super();
    }

    public DataNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataNotExistException(String message) {
        super(message);
    }

    public DataNotExistException(Throwable cause) {
        super(cause);
    }
}
