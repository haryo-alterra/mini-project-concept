package com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import lombok.*;

import java.io.Serializable;
import java.sql.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class JobApplicationEntityAdminDTO implements Serializable {
    @JsonProperty(value = "user_id")
    private UUID userId;

    @JsonProperty(value = "applicant_name")
    private String applicantName;

    private String message;

    @JsonProperty(value = "expected_salary")
    private int expectedSalary;

    @JsonProperty(value = "expected_join_date")
    private Date expectedJoinDate;

    @JsonProperty(value = "submitted_at")
    private Date createdAt;
}
