package com.jobsearchjobservice.jobapp.application.out.jobs;

public interface JobUserNameChangePort {
    void autoUpdateName(String userId, String newName);
}
