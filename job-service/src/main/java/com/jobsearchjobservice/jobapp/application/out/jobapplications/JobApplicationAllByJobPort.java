package com.jobsearchjobservice.jobapp.application.out.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityAdminDTO;
import org.springframework.data.domain.Pageable;

public interface JobApplicationAllByJobPort {
    PaginationJobAppEntityAdminDTO getAllApplicationByJob(
            Long jobId, Pageable pageable);
}
