package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.application.in.jobs.JobUserNameChangeUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobUserNameChangePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobUserNameChangeService implements JobUserNameChangeUseCase {
    private final JobUserNameChangePort jobUserNameChangePort;

    @Override
    public void autoUpdateName(String userId, String newName) {
        jobUserNameChangePort.autoUpdateName(userId, newName);
    }
}
