package com.jobsearchjobservice.jobapp.application.out.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityDTO;
import org.springframework.data.domain.Pageable;

public interface JobApplicationAllByUserPort {
    PaginationJobAppEntityDTO getAllApplicationByUser(String userId, Pageable pageable);
}
