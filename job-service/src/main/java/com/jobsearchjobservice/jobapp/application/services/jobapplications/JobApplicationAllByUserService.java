package com.jobsearchjobservice.jobapp.application.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationAllByUserUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationAllByUserPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobApplicationAllByUserService implements JobApplicationAllByUserUseCase {
    private final JobApplicationAllByUserPort jobAppByUserPort;

    @Override
    public PaginationJobAppEntityDTO getAllApplicationByUser(String userId, Pageable pageable) {
        return jobAppByUserPort.getAllApplicationByUser(userId, pageable);
    }

}
