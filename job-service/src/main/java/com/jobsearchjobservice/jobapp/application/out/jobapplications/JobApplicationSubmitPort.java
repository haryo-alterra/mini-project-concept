package com.jobsearchjobservice.jobapp.application.out.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;

public interface JobApplicationSubmitPort {
    JobApplicationDTO submitApplication(
            String userId, String fullName, JobApplicationDTO app);
}
