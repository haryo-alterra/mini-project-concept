package com.jobsearchjobservice.jobapp.application.out.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;

public interface JobDeletePort {
    JobDTO deleteJob(Long id);
}
