package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPutUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobPutPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobPutService implements JobPutUseCase {
    private final JobPutPort jobPutPort;

    @Override
    public JobDTO editJob(Long id, JobDTO job) {
        return jobPutPort.editJob(id, job);
    }
}
