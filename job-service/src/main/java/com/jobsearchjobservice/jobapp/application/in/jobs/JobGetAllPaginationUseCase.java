package com.jobsearchjobservice.jobapp.application.in.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsDTO;
import org.springframework.data.domain.Pageable;

public interface JobGetAllPaginationUseCase {
    PaginationJobsDTO getAllJobsPagination(Pageable pageable);
}
