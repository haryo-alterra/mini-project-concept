package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobGetAllPaginationUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobGetAllPaginationPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobGetAllPaginationService implements JobGetAllPaginationUseCase {
    private final JobGetAllPaginationPort jobGetAllPaginationPort;

    @Override
    public PaginationJobsDTO getAllJobsPagination(Pageable pageable) {
        return jobGetAllPaginationPort.getAllJobsPagination(pageable);
    }
}
