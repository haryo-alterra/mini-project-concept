package com.jobsearchjobservice.jobapp.application.in.jobs;

public interface JobUserNameChangeUseCase {
    void autoUpdateName(String userId, String newName);
}
