package com.jobsearchjobservice.jobapp.application.out.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;

public interface JobApplicationDeletePort {
    JobApplicationEntityDTO deleteApplication(String userId, Long jobId);
}
