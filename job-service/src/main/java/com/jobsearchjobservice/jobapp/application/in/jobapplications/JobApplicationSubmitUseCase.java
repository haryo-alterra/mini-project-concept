package com.jobsearchjobservice.jobapp.application.in.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;

public interface JobApplicationSubmitUseCase {
    JobApplicationDTO submitApplication(
            String userId, String fullName, JobApplicationDTO app);
}
