package com.jobsearchjobservice.jobapp.application.out.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;

public interface JobGetByIdPort {
    JobDTO getJob(Long id);
}
