package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPostUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobPostPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobPostService implements JobPostUseCase {
    private final JobPostPort jobPostPort;

    @Override
    public JobDTO postNewJob(JobDTO job) {
        return jobPostPort.postNewJob(job);
    }
}
