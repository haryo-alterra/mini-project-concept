package com.jobsearchjobservice.jobapp.application.in.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityAdminDTO;
import org.springframework.data.domain.Pageable;

public interface JobApplicationAllByJobUseCase {
    PaginationJobAppEntityAdminDTO getAllApplicationByJob(
            Long jobId, Pageable pageable);
}
