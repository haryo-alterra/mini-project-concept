package com.jobsearchjobservice.jobapp.application.out.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.PaginationJobsDTO;
import org.springframework.data.domain.Pageable;

public interface JobGetAllPaginationPort {
    PaginationJobsDTO getAllJobsPagination(Pageable pageable);
}
