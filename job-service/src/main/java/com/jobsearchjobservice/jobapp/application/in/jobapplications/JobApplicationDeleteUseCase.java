package com.jobsearchjobservice.jobapp.application.in.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;

public interface JobApplicationDeleteUseCase {
    JobApplicationEntityDTO deleteApplication(String userId, Long jobId);
}
