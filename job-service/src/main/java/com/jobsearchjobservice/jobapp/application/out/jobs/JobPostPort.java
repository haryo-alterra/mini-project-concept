package com.jobsearchjobservice.jobapp.application.out.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;

public interface JobPostPort {
    JobDTO postNewJob(JobDTO job);
}
