package com.jobsearchjobservice.jobapp.application.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationDeleteUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationDeletePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobApplicationDeleteService implements JobApplicationDeleteUseCase {
    private final JobApplicationDeletePort jobApplicationDeletePort;

    @Override
    public JobApplicationEntityDTO deleteApplication(String userId, Long jobId) {
        return jobApplicationDeletePort.deleteApplication(userId, jobId);
    }
}
