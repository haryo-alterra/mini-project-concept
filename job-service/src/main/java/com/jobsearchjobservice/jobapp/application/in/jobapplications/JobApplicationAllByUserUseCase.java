package com.jobsearchjobservice.jobapp.application.in.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityDTO;
import org.springframework.data.domain.Pageable;

public interface JobApplicationAllByUserUseCase {
    PaginationJobAppEntityDTO getAllApplicationByUser(String userId, Pageable pageable);
}
