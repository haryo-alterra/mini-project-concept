package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobDeleteUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobDeletePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobDeleteService implements JobDeleteUseCase {
    private final JobDeletePort jobDeletePort;

    @Override
    public JobDTO deleteJob(Long id) {
        return jobDeletePort.deleteJob(id);
    }
}
