package com.jobsearchjobservice.jobapp.application.in.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;

public interface JobPostUseCase {
    JobDTO postNewJob(JobDTO job);
}
