package com.jobsearchjobservice.jobapp.application.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.PaginationJobAppEntityAdminDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationAllByJobUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationAllByJobPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobApplicationAllByJobService
        implements JobApplicationAllByJobUseCase {
    private final JobApplicationAllByJobPort jobAppAllByJobPort;

    @Override
    public PaginationJobAppEntityAdminDTO getAllApplicationByJob(
            Long jobId, Pageable pageable) {
        return jobAppAllByJobPort.getAllApplicationByJob(jobId, pageable);
    }
}
