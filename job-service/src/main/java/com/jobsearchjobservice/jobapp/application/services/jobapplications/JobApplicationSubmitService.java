package com.jobsearchjobservice.jobapp.application.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationSubmitUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationSubmitPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobApplicationSubmitService implements JobApplicationSubmitUseCase {
    private final JobApplicationSubmitPort jobAppSubmitPort;

    @Override
    public JobApplicationDTO submitApplication(
            String userId, String fullName, JobApplicationDTO app) {
        return jobAppSubmitPort.submitApplication(userId, fullName, app);
    }
}
