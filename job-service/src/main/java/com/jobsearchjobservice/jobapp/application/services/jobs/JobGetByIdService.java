package com.jobsearchjobservice.jobapp.application.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobGetByIdUseCase;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobGetByIdPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobGetByIdService implements JobGetByIdUseCase {
    private final JobGetByIdPort jobGetByIdPort;

    @Override
    public JobDTO getJob(Long id) {
        return jobGetByIdPort.getJob(id);
    }
}
