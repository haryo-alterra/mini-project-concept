package com.jobsearchjobservice.jobapp.utils;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;

import java.sql.Date;

public class UpdateJobAttribute {
    private UpdateJobAttribute() {}

    public static JobEntity updateAttribute(JobEntity job, JobDTO req) {
        if (req.getTitle() != null) job.setTitle(req.getTitle());
        if (req.getDescription() != null) job.setDescription(req.getDescription());
        if (req.getApplicantCounts() != 0) job.setApplicantCounts(req.getApplicantCounts());
        if (req.getSalaryLow() != 0) job.setSalaryLow(req.getSalaryLow());
        if (req.getSalaryHigh() != 0) job.setSalaryHigh(req.getSalaryHigh());
        if (req.getExpiryDate() != null) job.setExpiredAt(Date.valueOf(req.getExpiryDate()));
        if (req.getId() != null) job.setId(req.getId());

        return job;
    }
}
