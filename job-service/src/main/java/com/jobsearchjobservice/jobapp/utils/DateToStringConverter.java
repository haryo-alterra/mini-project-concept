package com.jobsearchjobservice.jobapp.utils;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateToStringConverter {
    private DateToStringConverter() {}

    public static String convert(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }
}
