package com.jobsearchjobservice.jobapp.utils;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import org.modelmapper.ModelMapper;

public class EntityToDTOJobConverter {
    private EntityToDTOJobConverter() {}

    public static JobDTO convert(JobEntity jobEntity) {
        ModelMapper modelMapper = new ModelMapper();
        JobDTO jobDTO = modelMapper.map(jobEntity, JobDTO.class);
        jobDTO.setExpiryDate(
                DateToStringConverter.convert(jobEntity.getExpiredAt()));
        return jobDTO;
    }
}
