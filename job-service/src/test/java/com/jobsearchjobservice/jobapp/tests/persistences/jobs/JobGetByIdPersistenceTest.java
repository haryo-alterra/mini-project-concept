package com.jobsearchjobservice.jobapp.tests.persistences.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.utils.DateToStringConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobGetByIdPersistenceTest {
    @Mock
    JobRepository jobRepository;

    @InjectMocks
    JobPersistence jobPersistence;

    @Spy
    ModelMapper modelMapper;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given invalid job id during retrieval, should return exception")
    public void givenInvalidJobId_ShouldReturnException() {
        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.empty());
        assertThrows(JobNotFoundException.class, () ->
                jobPersistence.getJob((long) 6));
    }

    @Test
    @DisplayName("Given valid request to get specific job data, should do it")
    public void givenValidJobRetrievalRequest_ShouldSucceed() {
        Long jobId = (long) 1;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        JobDTO generatedJob = jobPersistence.getJob(jobId);

        assertThat(generatedJob.getId()).isEqualTo(jobId);
        assertThat(generatedJob.getTitle()).isEqualTo(job.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(job.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(job.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(job.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate()).isEqualTo(job.getExpiryDate());
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(job.getApplicantCounts());
    }
}
