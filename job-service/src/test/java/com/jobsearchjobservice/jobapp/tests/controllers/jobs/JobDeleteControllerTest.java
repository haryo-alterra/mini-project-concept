package com.jobsearchjobservice.jobapp.tests.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.controllers.jobs.JobDeleteController;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobDeleteUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobDeleteController.class)
public class JobDeleteControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobDeleteUseCase jobDeleteUseCase;

    @MockBean
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid delete job request, should return 200")
    public void givenValidPostJobRequest_ShouldReturn201() throws Exception {
        Long jobId = (long) 11;

        mockMvc.perform(delete("/api/v1/jobs/"+jobId))
                .andExpect(status().isOk());

        then(jobDeleteUseCase).should().deleteJob(jobId);
    }
}
