package com.jobsearchjobservice.jobapp.tests.persistences.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.utils.DateToStringConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobDeletePersistenceTest {
    @Mock
    JobRepository jobRepository;

    @InjectMocks
    JobPersistence jobPersistence;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given invalid job id during deletion, should return exception")
    public void givenInvalidJobId_ShouldReturnException() {
        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.empty());
        assertThrows(JobNotFoundException.class, () ->
                jobPersistence.deleteJob((long) 6));
    }

    @Test
    @DisplayName("Given valid request to delete a job, should delete it")
    public void givenValidDeleteRequest_ShouldDeleteJobSuccessfully() {
        Mockito.doNothing().when(jobRepository).delete(Mockito.any(JobEntity.class));
        Long jobId = (long) 1;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        jobPersistence.deleteJob(jobId);
        Mockito.verify(jobRepository).delete(Mockito.any(JobEntity.class));
    }
}
