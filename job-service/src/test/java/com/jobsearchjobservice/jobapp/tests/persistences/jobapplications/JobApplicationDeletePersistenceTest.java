package com.jobsearchjobservice.jobapp.tests.persistences.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobApplicationEntity;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobApplicationNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobApplicationPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobApplicationRepository;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobApplicationDeletePersistenceTest {
    @Mock
    JobRepository jobRepository;

    @Mock
    JobApplicationRepository jobAppRepository;

    @InjectMocks
    JobApplicationPersistence jobAppPersistence;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given invalid job application reference, should return exception")
    public void givenInvalidJobApplicationReference_ShouldReturnException() {
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        Mockito.when(jobAppRepository.findByJobAndUserId(
                        Mockito.anyLong(), Mockito.any(UUID.class)))
                .thenReturn(Optional.empty());

        assertThrows(JobApplicationNotFoundException.class, () ->
                jobAppPersistence.deleteApplication(userId, jobId));
    }

    @Test
    @DisplayName("Given valid request to delete a user application, should do it successfully")
    public void givenValidRequestToDeleteApplication_ShouldDoIt() {
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        JobDTO job = new JobDTO();
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2025-01-01");

        JobEntity referredJob = modelMapper.map(job, JobEntity.class);
        referredJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobApplicationEntity deletedApplication = new JobApplicationEntity();
        deletedApplication.setApplicantName("Tom");
        deletedApplication.setMessage("Hai sayang");
        deletedApplication.setExpectedSalary(20000000);
        deletedApplication.setExpectedJoinDate(Date.valueOf("2024-01-01"));
        deletedApplication.setCreatedAt(Date.valueOf("2023-01-01"));
        deletedApplication.setJob(referredJob);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(referredJob));
        Mockito.when(jobAppRepository.findByJobAndUserId(
                Mockito.anyLong(), Mockito.any(UUID.class)))
                .thenReturn(Optional.of(deletedApplication));

        JobApplicationEntityDTO generatedDTO
                = jobAppPersistence.deleteApplication(userId, jobId);

        assertThat(generatedDTO.getJob()).isEqualTo(deletedApplication.getJob());
        assertThat(generatedDTO.getApplicantName()).isEqualTo(deletedApplication.getApplicantName());
        assertThat(generatedDTO.getCreatedAt()).isEqualTo(deletedApplication.getCreatedAt());
        assertThat(generatedDTO.getMessage()).isEqualTo(deletedApplication.getMessage());
        assertThat(generatedDTO.getExpectedSalary()).isEqualTo(deletedApplication.getExpectedSalary());
        assertThat(generatedDTO.getExpectedJoinDate()).isEqualTo(deletedApplication.getExpectedJoinDate());
    }
}
