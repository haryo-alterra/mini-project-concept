package com.jobsearchjobservice.jobapp.tests.persistences.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobApplicationEntity;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidDateException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidSalaryRangeException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobApplicationExistsException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobApplicationPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobApplicationRepository;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobApplicationSubmitPersistenceTest {
    @Mock
    JobRepository jobRepository;

    @Mock
    JobApplicationRepository jobAppRepository;

    @InjectMocks
    JobApplicationPersistence jobAppPersistence;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given deadline date is in the past (before today), should return exception")
    public void givenDateForDeadlineAlreadyPassed_ShouldReturnException() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 13;

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("1998-10-02");
        resultingApplication.setExpectedSalary(10000000);

        assertThrows(InvalidDateException.class, () ->
                jobAppPersistence.submitApplication(
                        userId, name, resultingApplication));
    }

    @Test
    @DisplayName("Given negative salary, should return exception")
    public void givenNegativeSalary_ShouldReturnException() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 13;

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2025-10-02");
        resultingApplication.setExpectedSalary(-10000000);

        assertThrows(InvalidSalaryRangeException.class, () ->
                jobAppPersistence.submitApplication(
                        userId, name, resultingApplication));
    }

    @Test
    @DisplayName("Given invalid job id during submission, should return exception")
    public void givenInvalidJobId_ShouldReturnException() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 13;

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2025-10-02");
        resultingApplication.setExpectedSalary(10000000);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.empty());
        assertThrows(JobNotFoundException.class, () ->
                jobAppPersistence.submitApplication(
                        userId, name, resultingApplication));
    }

    @Test
    @DisplayName("Given submission already exists, should return exception")
    public void givenSubmissionAlreadyExists_ShouldReturnException() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        JobDTO job = new JobDTO();
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2025-10-02");
        resultingApplication.setExpectedSalary(10000000);

        JobApplicationEntity resultingJobAppEntity = modelMapper.map(
                resultingApplication, JobApplicationEntity.class);
        resultingJobAppEntity.setId((long) 12);
        resultingJobAppEntity.setJob(foundJob);
        resultingJobAppEntity.setApplicantName(name);
        resultingJobAppEntity.setUserId(UUID.fromString(userId));

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        Mockito.when(jobAppRepository.findByJobAndUserId(
                Mockito.anyLong(), Mockito.any(UUID.class)))
                .thenReturn(Optional.of(resultingJobAppEntity));

        assertThrows(JobApplicationExistsException.class, () ->
                jobAppPersistence.submitApplication(
                        userId, name, resultingApplication));
    }

    @Test
    @DisplayName("Given valid request to post submission, should do it successfully")
    public void givenValidRequestToPostSubmission_ShouldDoIt() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        JobDTO job = new JobDTO();
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2025-10-02");
        resultingApplication.setExpectedSalary(10000000);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        Mockito.when(jobAppRepository.findByJobAndUserId(
                        Mockito.anyLong(), Mockito.any(UUID.class)))
                .thenReturn(Optional.empty());

        JobApplicationDTO generatedApplication = jobAppPersistence
                .submitApplication(userId, name, resultingApplication);

        Mockito.verify(jobAppRepository).saveNewJobApplication(
                Mockito.any(UUID.class), Mockito.anyString(), Mockito.anyLong(),
                Mockito.anyString(), Mockito.anyInt(), Mockito.any(Date.class));

        assertThat(generatedApplication.getJobId()).isEqualTo(resultingApplication.getJobId());
        assertThat(generatedApplication.getExpectedJoinDate())
                .isEqualTo(resultingApplication.getExpectedJoinDate());
        assertThat(generatedApplication.getExpectedSalary())
                .isEqualTo(resultingApplication.getExpectedSalary());
        assertThat(generatedApplication.getMessage())
                .isEqualTo(resultingApplication.getMessage());
    }
}
