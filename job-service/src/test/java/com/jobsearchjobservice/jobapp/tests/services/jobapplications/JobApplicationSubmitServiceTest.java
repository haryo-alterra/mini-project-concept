package com.jobsearchjobservice.jobapp.tests.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationSubmitPort;
import com.jobsearchjobservice.jobapp.application.services.jobapplications.JobApplicationSubmitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JobApplicationSubmitServiceTest {
    @Mock
    JobApplicationSubmitPort jobAppSubmitPort;

    @InjectMocks
    JobApplicationSubmitService jobAppSubmitService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid request to submit job application, should do it successfully")
    public void givenValidRequestToSubmitApplication_ShouldSuccessfullyDoIt() {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 13;

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2029-10-02");
        resultingApplication.setExpectedSalary(10000000);

        Mockito.when(jobAppSubmitPort.submitApplication(
                Mockito.anyString(), Mockito.anyString(), Mockito.any(JobApplicationDTO.class)))
                .thenReturn(resultingApplication);
        JobApplicationDTO generatedApplication = jobAppSubmitService.submitApplication(
                userId, name, resultingApplication
        );

        assertThat(generatedApplication.getJobId()).isEqualTo(resultingApplication.getJobId());
        assertThat(generatedApplication.getExpectedJoinDate())
                .isEqualTo(resultingApplication.getExpectedJoinDate());
        assertThat(generatedApplication.getExpectedSalary())
                .isEqualTo(resultingApplication.getExpectedSalary());
        assertThat(generatedApplication.getMessage())
                .isEqualTo(resultingApplication.getMessage());
    }
}
