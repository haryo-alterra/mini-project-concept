package com.jobsearchjobservice.jobapp.tests.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.services.jobs.JobPutService;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobPutPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JobPutServiceTest {
    @Mock
    JobPutPort jobPutPort;

    @InjectMocks
    JobPutService jobPutService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a request to edit a job, should do it successfully")
    public void givenRequestToEditJob_ShouldDoItSuccessfully() {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        JobDTO updatedJob = job;
        updatedJob.setTitle("Junior Backend Engineer");
        updatedJob.setSalaryLow(6000000);
        updatedJob.setSalaryHigh(8000000);

        JobDTO request = new JobDTO();
        request.setTitle("Junior Backend Engineer");
        request.setSalaryLow(6000000);
        request.setSalaryHigh(8000000);

        Mockito.when(jobPutPort.editJob(Mockito.anyLong(), Mockito.any(JobDTO.class)))
                .thenReturn(updatedJob);
        JobDTO generatedJob = jobPutService.editJob(jobId, request);

        assertThat(generatedJob.getId()).isEqualTo(updatedJob.getId());
        assertThat(generatedJob.getTitle()).isEqualTo(updatedJob.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(updatedJob.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(updatedJob.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(updatedJob.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate()).isEqualTo(updatedJob.getExpiryDate());
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(updatedJob.getApplicantCounts());
    }
}
