package com.jobsearchjobservice.jobapp.tests.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.services.jobs.JobPostService;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobPostPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JobPostServiceTest {
    @Mock
    JobPostPort jobPostPort;

    @InjectMocks
    JobPostService jobPostService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a request to post new job, should do it successfully")
    public void givenRequestToPostJob_ShouldDoItSuccessfully() {
        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        JobDTO newJob = job;
        newJob.setId((long) 1);

        Mockito.when(jobPostPort.postNewJob(job)).thenReturn(newJob);
        JobDTO generatedJob = jobPostService.postNewJob(job);

        assertThat(generatedJob.getId()).isEqualTo(newJob.getId());
        assertThat(generatedJob.getTitle()).isEqualTo(newJob.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(newJob.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(newJob.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(newJob.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate()).isEqualTo(newJob.getExpiryDate());
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(newJob.getApplicantCounts());
    }
}