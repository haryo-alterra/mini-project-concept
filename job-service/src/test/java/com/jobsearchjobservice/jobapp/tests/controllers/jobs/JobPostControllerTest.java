package com.jobsearchjobservice.jobapp.tests.controllers.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchjobservice.jobapp.adapters.controllers.jobs.JobPostController;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPostUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobPostController.class)
public class JobPostControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobPostUseCase jobPostUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Spy
    ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid post job request, should return 201")
    public void givenValidPostJobRequest_ShouldReturn201() throws Exception {
        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(5000000);
        job.setSalaryHigh(10000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        mockMvc.perform(post("/api/v1/jobs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(job)))
                .andExpect(status().isCreated());

        then(jobPostUseCase).should().postNewJob(job);
    }
}
