package com.jobsearchjobservice.jobapp.tests.controllers.jobapplications;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications.JobApplicationDeleteController;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationDeleteUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobApplicationDeleteController.class)
public class JobApplicationDeleteControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobApplicationDeleteUseCase jobAppDeleteUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Spy
    ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid deletion request, should return 200")
    public void givenValidDeletionRequest_ShouldReturn200() throws Exception {
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        mockMvc.perform(delete("/api/v1/user-application/" + jobId)
                .header("user_id", userId))
                .andExpect(status().isOk());

        then(jobAppDeleteUseCase).should().deleteApplication(userId, jobId);
    }
}
