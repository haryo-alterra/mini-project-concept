package com.jobsearchjobservice.jobapp.tests.services.jobapplications;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationEntityDTO;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.application.out.jobapplications.JobApplicationDeletePort;
import com.jobsearchjobservice.jobapp.application.services.jobapplications.JobApplicationDeleteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JobApplicationDeleteServiceTest {
    @Mock
    JobApplicationDeletePort jobAppDeletePort;

    @InjectMocks
    JobApplicationDeleteService jobAppDeleteService;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to delete existing job application, should do it successfully")
    public void givenRequestToDelete_ShouldDoIt() {
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        JobDTO job = new JobDTO();
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2025-01-01");

        JobEntity referredJob = modelMapper.map(job, JobEntity.class);
        referredJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobApplicationEntityDTO deletedApplication = new JobApplicationEntityDTO();
        deletedApplication.setApplicantName("Tom");
        deletedApplication.setMessage("Hai sayang");
        deletedApplication.setExpectedSalary(20000000);
        deletedApplication.setExpectedJoinDate(Date.valueOf("2024-01-01"));
        deletedApplication.setCreatedAt(Date.valueOf("2023-01-01"));
        deletedApplication.setJob(referredJob);

        Mockito.when(jobAppDeletePort.deleteApplication(Mockito.anyString(), Mockito.anyLong()))
                .thenReturn(deletedApplication);

        JobApplicationEntityDTO generatedApplication
                = jobAppDeleteService.deleteApplication(userId, jobId);

        assertThat(generatedApplication.getJob()).isEqualTo(deletedApplication.getJob());
        assertThat(generatedApplication.getApplicantName()).isEqualTo(deletedApplication.getApplicantName());
        assertThat(generatedApplication.getCreatedAt()).isEqualTo(deletedApplication.getCreatedAt());
        assertThat(generatedApplication.getMessage()).isEqualTo(deletedApplication.getMessage());
        assertThat(generatedApplication.getExpectedSalary()).isEqualTo(deletedApplication.getExpectedSalary());
        assertThat(generatedApplication.getExpectedJoinDate()).isEqualTo(deletedApplication.getExpectedJoinDate());
    }
}
