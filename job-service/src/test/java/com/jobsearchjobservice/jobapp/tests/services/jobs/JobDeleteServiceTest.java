package com.jobsearchjobservice.jobapp.tests.services.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.services.jobs.JobDeleteService;
import com.jobsearchjobservice.jobapp.application.out.jobs.JobDeletePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class JobDeleteServiceTest {
    @Mock
    JobDeletePort jobDeletePort;

    @InjectMocks
    JobDeleteService jobDeleteService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a request to delete an existing job, should do it successfully")
    public void givenRequestToDeleteJob_ShouldDoItSuccessfully() {
        JobDTO job = new JobDTO();
        Long jobId = (long) 6;
        job.setId(jobId);
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        Mockito.when(jobDeletePort.deleteJob(Mockito.anyLong()))
                .thenReturn(job);

        JobDTO generatedJob = jobDeleteService.deleteJob(jobId);

        assertThat(generatedJob.getId()).isEqualTo(job.getId());
        assertThat(generatedJob.getTitle()).isEqualTo(job.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(job.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(job.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(job.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate()).isEqualTo(job.getExpiryDate());
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(job.getApplicantCounts());
    }
}
