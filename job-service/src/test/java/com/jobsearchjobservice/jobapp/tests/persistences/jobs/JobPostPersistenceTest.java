package com.jobsearchjobservice.jobapp.tests.persistences.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidDateException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidSalaryRangeException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.utils.DateToStringConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobPostPersistenceTest {
    @Mock
    JobRepository jobRepository;

    @InjectMocks
    JobPersistence jobPersistence;

    @Spy
    ModelMapper modelMapper;

    @Spy
    DateToStringConverter date2String;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given deadline date is in the past (before today), should return exception")
    public void givenDateForDeadlineAlreadyPassed_ShouldReturnException() {
        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(5000000);
        job.setSalaryHigh(10000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2019-03-03");

        assertThrows(InvalidDateException.class, () ->
                jobPersistence.postNewJob(job));
    }

    @Test
    @DisplayName("Given lower bound for salary is higher than its upper bound, should return exception")
    public void givenLowerBoundSalaryHigher_ShouldReturnException() {
        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(0);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        assertThrows(InvalidSalaryRangeException.class, () ->
                jobPersistence.postNewJob(job));
    }

    @Test
    @DisplayName("Given negative salary, should return exception")
    public void givenNegativeSalary_ShouldReturnException() {
        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(-999);
        job.setSalaryHigh(9990000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        assertThrows(InvalidSalaryRangeException.class, () ->
                jobPersistence.postNewJob(job));
    }

    @Test
    @DisplayName("Given valid request to post a job, should do it successfully")
    public void givenValidPostJobRequest_ShouldDoItSuccessfully() {
        JobDTO job = new JobDTO();;
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(5000000);
        job.setSalaryHigh(10000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2023-03-03");

        JobEntity newJob = modelMapper.map(job, JobEntity.class);
        newJob.setId((long) 1);
        newJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        Mockito.when(jobRepository.save(Mockito.any(JobEntity.class)))
                .thenReturn(newJob);

        JobDTO generatedJob = jobPersistence.postNewJob(job);
        assertThat(generatedJob.getId()).isEqualTo(newJob.getId());
        assertThat(generatedJob.getTitle()).isEqualTo(newJob.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(newJob.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(newJob.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(newJob.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate())
                .isEqualTo(date2String.convert(newJob.getExpiredAt()));
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(newJob.getApplicantCounts());
    }
}
