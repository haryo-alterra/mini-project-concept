package com.jobsearchjobservice.jobapp.tests.controllers.jobs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchjobservice.jobapp.adapters.controllers.jobs.JobPutController;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobPutUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobPutController.class)
public class JobPutControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobPutUseCase jobPutUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Spy
    ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid put job request, should return 200")
    public void givenValidPutJobRequest_ShouldReturn200() throws Exception {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setSalaryLow(5000000);
        job.setSalaryHigh(10000000);

        mockMvc.perform(put("/api/v1/jobs/"+jobId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(job)))
                .andExpect(status().isOk());

        then(jobPutUseCase).should().editJob(jobId, job);
    }
}
