package com.jobsearchjobservice.jobapp.tests.persistences.jobs;

import com.jobsearchjobservice.jobapp.adapters.dtos.jobs.JobDTO;
import com.jobsearchjobservice.jobapp.adapters.entities.JobEntity;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidDateException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.InvalidSalaryRangeException;
import com.jobsearchjobservice.jobapp.adapters.exceptions.JobNotFoundException;
import com.jobsearchjobservice.jobapp.adapters.persistences.JobPersistence;
import com.jobsearchjobservice.jobapp.adapters.repositories.JobRepository;
import com.jobsearchjobservice.jobapp.utils.DateToStringConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.sql.Date;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class JobPutPersistenceTest {
    @Mock
    JobRepository jobRepository;

    @InjectMocks
    JobPersistence jobPersistence;

    @Spy
    ModelMapper modelMapper;

    @Spy
    DateToStringConverter date2String;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given invalid job id during retrieval, should return exception")
    public void givenInvalidJobId_ShouldReturnException() {
        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.empty());
        assertThrows(JobNotFoundException.class, () ->
                jobPersistence.getJob((long) 6));
    }

    @Test
    @DisplayName("Given deadline date is in the past (before today), should return exception")
    public void givenDateForDeadlineAlreadyPassed_ShouldReturnException() {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2022-01-01");

        JobDTO updatedJob = job;
        updatedJob.setTitle("Junior Backend Engineer");
        updatedJob.setSalaryLow(6000000);
        updatedJob.setSalaryHigh(8000000);

        JobDTO request = new JobDTO();
        request.setTitle("Junior Backend Engineer");
        request.setSalaryLow(6000000);
        request.setSalaryHigh(8000000);

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        assertThrows(InvalidDateException.class, () ->
                jobPersistence.editJob((long) 13, request));
    }

    @Test
    @DisplayName("Given negative salary, should return exception")
    public void givenNegativeSalary_ShouldReturnException() {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2024-05-01");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobDTO request = new JobDTO();
        request.setTitle("Junior Backend Engineer");
        request.setSalaryLow(-700);
        request.setSalaryHigh(10000);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        assertThrows(InvalidSalaryRangeException.class, () ->
                jobPersistence.editJob((long) 13, request));
    }

    @Test
    @DisplayName("Given lower bound for salary is higher than its upper bound, should return exception")
    public void givenLowerBoundSalaryHigher_ShouldReturnException() {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2024-05-01");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobDTO request = new JobDTO();
        request.setTitle("Junior Backend Engineer");
        request.setSalaryLow(50000);
        request.setSalaryHigh(10000);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        assertThrows(InvalidSalaryRangeException.class, () ->
                jobPersistence.editJob((long) 13, request));
    }

    @Test
    @DisplayName("Given valid request to edit a job, should do it successfully")
    public void givenValidEditJobRequest_ShouldDoItSuccessfully() {
        Long jobId = (long) 13;

        JobDTO job = new JobDTO();
        job.setTitle("Backend Engineer");
        job.setDescription("Mock job description");
        job.setSalaryLow(10000000);
        job.setSalaryHigh(15000000);
        job.setApplicantCounts(0);
        job.setExpiryDate("2024-05-01");

        JobEntity foundJob = modelMapper.map(job, JobEntity.class);
        foundJob.setId(jobId);
        foundJob.setExpiredAt(Date.valueOf(job.getExpiryDate()));

        JobEntity updatedJob = foundJob;
        updatedJob.setTitle("Junior Backend Engineer");
        updatedJob.setSalaryLow(1000000);
        updatedJob.setSalaryHigh(5000000);

        JobDTO request = new JobDTO();
        request.setTitle("Junior Backend Engineer");
        request.setSalaryLow(1000000);
        request.setSalaryHigh(5000000);

        Mockito.when(jobRepository.findById(Mockito.anyLong()))
                .thenReturn(Optional.of(foundJob));
        Mockito.when(jobRepository.save(Mockito.any(JobEntity.class)))
                .thenReturn(updatedJob);
        JobDTO generatedJob = jobPersistence.editJob(jobId, request);

        assertThat(generatedJob.getId()).isEqualTo(updatedJob.getId());
        assertThat(generatedJob.getTitle()).isEqualTo(updatedJob.getTitle());
        assertThat(generatedJob.getDescription()).isEqualTo(updatedJob.getDescription());
        assertThat(generatedJob.getSalaryLow()).isEqualTo(updatedJob.getSalaryLow());
        assertThat(generatedJob.getSalaryHigh()).isEqualTo(updatedJob.getSalaryHigh());
        assertThat(generatedJob.getExpiryDate()).isEqualTo(
                date2String.convert(updatedJob.getExpiredAt()));
        assertThat(generatedJob.getApplicantCounts())
                .isEqualTo(updatedJob.getApplicantCounts());
    }
}
