package com.jobsearchjobservice.jobapp.tests.controllers.jobs;

import com.jobsearchjobservice.jobapp.adapters.controllers.jobs.JobGetByIdController;
import com.jobsearchjobservice.jobapp.application.in.jobs.JobGetByIdUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobGetByIdController.class)
public class JobGetByIdControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobGetByIdUseCase jobGetByIdUseCase;

    @MockBean
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid delete job request, should return 200")
    public void givenValidPostJobRequest_ShouldReturn201() throws Exception {
        Long jobId = (long) 11;

        mockMvc.perform(get("/api/v1/jobs/"+jobId))
                .andExpect(status().isOk());

        then(jobGetByIdUseCase).should().getJob(jobId);
    }
}
