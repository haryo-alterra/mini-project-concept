package com.jobsearchjobservice.jobapp.tests.controllers.jobapplications;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchjobservice.jobapp.adapters.controllers.jobapplications.JobApplicationSubmitController;
import com.jobsearchjobservice.jobapp.adapters.dtos.jobapplications.JobApplicationDTO;
import com.jobsearchjobservice.jobapp.application.in.jobapplications.JobApplicationSubmitUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = JobApplicationSubmitController.class)
public class JobApplicationSubmitControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    JobApplicationSubmitUseCase jobAppSubmitUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Spy
    ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid post submission request, should return 201")
    public void givenValidPostSubmissionRequest_ShouldReturn201() throws Exception {
        String name = "Fulan";
        String userId = "53766544-725c-4dc8-9981-4a3ca681a47d";
        Long jobId = (long) 6;

        JobApplicationDTO resultingApplication = new JobApplicationDTO();
        resultingApplication.setJobId(jobId);
        resultingApplication.setMessage("Halo");
        resultingApplication.setExpectedJoinDate("2025-10-02");
        resultingApplication.setExpectedSalary(10000000);

        mockMvc.perform(post("/api/v1/user-application")
                .header("user_id", userId)
                .header("name", name)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper
                        .writeValueAsString(resultingApplication)))
                .andExpect(status().isCreated());

        then(jobAppSubmitUseCase).should().submitApplication(
                userId, name, resultingApplication);
    }
}
