package com.jobsearchdiscovery.discoveryapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class DiscoveryappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryappApplication.class, args);
	}

}
