package com.jobsearchfileservice.fileapp.utils;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;

public class FileToEntityGenerator {
    public static UserFileEntity generate(String userId, String fileName, byte[] fileBytes) {
        return new UserFileEntity(userId, fileName, new Binary(BsonBinarySubType.BINARY, fileBytes));
    }
}
