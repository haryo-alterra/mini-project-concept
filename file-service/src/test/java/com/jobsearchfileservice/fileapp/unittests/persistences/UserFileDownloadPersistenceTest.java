package com.jobsearchfileservice.fileapp.unittests.persistences;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.adapters.exceptions.UserFileNotFoundException;
import com.jobsearchfileservice.fileapp.adapters.persistences.UserFilePersistence;
import com.jobsearchfileservice.fileapp.adapters.repositories.UserFileRepository;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserFileDownloadPersistenceTest {
    @Mock
    UserFileRepository userFileRepository;

    @InjectMocks
    UserFilePersistence userFilePersistence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given file belonging to a given user ID not existing, should return exception")
    public void givenUserFileNotExisting_ShouldReturnException() {
        Mockito.when(userFileRepository.findByUserId(Mockito.anyString()))
                        .thenReturn(Optional.empty());
        assertThrows(UserFileNotFoundException.class, () ->
                userFilePersistence.downloadFile("9f966e77-8367-41e7-9a8a-0271a3670f5a"));
    }

    @Test
    @DisplayName("Given request to retrieve file by user ID, should do it successfully")
    public void givenValidDataRetrievalRequest_ShouldReturnData() throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        UserFileEntity userFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileRepository.findByUserId(Mockito.anyString()))
                .thenReturn(Optional.of(userFile));

        UserFileEntity generatedUserFile = userFilePersistence.downloadFile(userId);

        assertThat(generatedUserFile.getUserId()).isEqualTo(userFile.getUserId());
        assertThat(generatedUserFile.getFileName()).isEqualTo(userFile.getFileName());
        assertThat(generatedUserFile.getFile()).isEqualTo(userFile.getFile());
    }
}
