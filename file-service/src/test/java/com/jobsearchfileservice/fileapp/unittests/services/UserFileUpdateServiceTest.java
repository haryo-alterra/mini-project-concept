package com.jobsearchfileservice.fileapp.unittests.services;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.UserFileUpdateService;
import com.jobsearchfileservice.fileapp.application.out.UserFileUpdatePort;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileUpdateServiceTest {
    @Mock
    UserFileUpdatePort userFileUpdatePort;

    @InjectMocks
    UserFileUpdateService userFileUpdateService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to update registered user file, should update successfully")
    public void givenRequestToUpdateFile_ShouldUpdateSuccessfully()
            throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        String newFileName = "14-GreedyAlgorithms.pdf";
        byte[] fileBytes = generator.generate(fileName);
        byte[] newFileBytes = generator.generate(newFileName);
        UserFileEntity newUserFile = FileToEntityGenerator.generate(userId, newFileName, newFileBytes);

        Mockito.when(userFileUpdatePort.updateFile(
                        Mockito.anyString(), Mockito.anyString(), Mockito.any(byte[].class)))
                .thenReturn(newUserFile);

        UserFileEntity generatedUserFile = userFileUpdateService.updateFile(
                userId, fileName, fileBytes);

        assertThat(generatedUserFile.getUserId()).isEqualTo(newUserFile.getUserId());
        assertThat(generatedUserFile.getFileName()).isEqualTo(newUserFile.getFileName());
        assertThat(generatedUserFile.getFile()).isEqualTo(newUserFile.getFile());
    }
}
