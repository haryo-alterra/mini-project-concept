package com.jobsearchfileservice.fileapp.unittests.controllers;

import com.jobsearchfileservice.fileapp.adapters.controllers.UserFileDownloadController;
import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileDownloadUseCase;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileDownloadController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserFileDownloadControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileDownloadUseCase userFileDownloadUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Test
    @DisplayName("Given request to download file, should return 200")
    public void givenRequestToDownloadFile_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        UserFileEntity userFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileDownloadUseCase.downloadFile(Mockito.anyString()))
                .thenReturn(userFile);
        mockMvc.perform(get("/user_files/"+userId))
                .andExpect(status().isOk());
    }
}
