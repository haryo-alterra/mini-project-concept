package com.jobsearchfileservice.fileapp.unittests.controllers;

import com.jobsearchfileservice.fileapp.adapters.controllers.UserFileUpdateController;
import com.jobsearchfileservice.fileapp.application.in.UserFileUpdateUseCase;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Base64;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileUpdateController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserFileUpdateControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileUpdateUseCase userFileUpdateUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Test
    @DisplayName("Given request to update file, should return 200")
    public void givenRequestToUpdateFile_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        String base64File = Base64.getEncoder().encodeToString(fileBytes);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("file_name", fileName);
        body.add("file", base64File);

        mockMvc.perform(put("/user_files/"+userId)
                .header("Content-Type", "multipart/form-data")
                .params(body)).andExpect(status().isOk());

        then(userFileUpdateUseCase).should()
                .updateFile(userId, fileName, fileBytes);
    }
}
