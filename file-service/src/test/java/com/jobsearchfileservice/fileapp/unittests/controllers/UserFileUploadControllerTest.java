package com.jobsearchfileservice.fileapp.unittests.controllers;

import com.jobsearchfileservice.fileapp.adapters.controllers.UserFileUploadController;
import com.jobsearchfileservice.fileapp.application.in.UserFileUploadUseCase;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Base64;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileUploadController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserFileUploadControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileUploadUseCase userFileUploadUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Test
    @DisplayName("Given request to upload file, should return 201")
    public void givenRequestToUploadFile_ShouldReturn201()
            throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        String base64File = Base64.getEncoder().encodeToString(fileBytes);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("user_id", userId);
        body.add("file_name", fileName);
        body.add("file", base64File);

        mockMvc.perform(post("/user_files")
                .header("Content-Type", "multipart/form-data")
                .params(body)).andExpect(status().isCreated());

        then(userFileUploadUseCase).should()
                .uploadFile(userId, fileName, fileBytes);
    }
}
