package com.jobsearchfileservice.fileapp.unittests.services;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.UserFileDeleteService;
import com.jobsearchfileservice.fileapp.application.out.UserFileDeletePort;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDeleteServiceTest {
    @Mock
    UserFileDeletePort userFileDeletePort;

    @InjectMocks
    UserFileDeleteService userFileDeleteService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to delete file, should delete it successfully")
    public void givenRequestToDeleteFile_ShouldDeleteItSuccessfully() throws
            IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        UserFileEntity userFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileDeletePort.deleteFile(Mockito.anyString()))
                .thenReturn(userFile);
        UserFileEntity deletedUserFile = userFileDeleteService.deleteFile(userId);

        assertThat(deletedUserFile.getUserId()).isEqualTo(userFile.getUserId());
        assertThat(deletedUserFile.getFileName()).isEqualTo(userFile.getFileName());
        assertThat(deletedUserFile.getFile()).isEqualTo(userFile.getFile());
    }
}
