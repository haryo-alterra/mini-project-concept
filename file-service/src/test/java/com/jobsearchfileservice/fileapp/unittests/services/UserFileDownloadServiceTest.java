package com.jobsearchfileservice.fileapp.unittests.services;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.UserFileDownloadService;
import com.jobsearchfileservice.fileapp.application.out.UserFileDownloadPort;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDownloadServiceTest {
    @Mock
    UserFileDownloadPort userFileDownloadPort;

    @InjectMocks
    UserFileDownloadService userFileDownloadService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a request to retrieve file from MongoDB, should do it successfully")
    public void givenRequestToRetrieveFile_ShouldDownloadSuccessfully()
            throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        UserFileEntity userFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileDownloadPort.downloadFile(Mockito.anyString()))
                .thenReturn(userFile);
        UserFileEntity generatedFile = userFileDownloadService.downloadFile(userId);

        assertThat(generatedFile.getUserId()).isEqualTo(userFile.getUserId());
        assertThat(generatedFile.getFileName()).isEqualTo(userFile.getFileName());
        assertThat(generatedFile.getFile()).isEqualTo(userFile.getFile());
    }
}
