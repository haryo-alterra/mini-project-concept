package com.jobsearchfileservice.fileapp.unittests.persistences;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.adapters.exceptions.UserFileAlreadyExistsException;
import com.jobsearchfileservice.fileapp.adapters.persistences.UserFilePersistence;
import com.jobsearchfileservice.fileapp.adapters.repositories.UserFileRepository;
import com.jobsearchfileservice.fileapp.utils.FileBinaryGenerator;
import com.jobsearchfileservice.fileapp.utils.FileToEntityGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserFileUploadPersistenceTest {
    @Mock
    UserFileRepository userFileRepository;

    @InjectMocks
    UserFilePersistence userFilePersistence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given user file already registered, return exception")
    public void givenUserFileAlreadyRegistered_ShouldReturnException() throws
            IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileEntity foundUserFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileRepository.findByUserId(Mockito.anyString()))
                .thenReturn(Optional.of(foundUserFile));
        assertThrows(UserFileAlreadyExistsException.class, () ->
                userFilePersistence.uploadFile(userId, fileName, fileBytes));
    }

    @Test
    @DisplayName("Given a new user file, then should upload successfully")
    public void givenANewUserFileUpload_thenShouldUploadSuccessfully() throws
            IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileEntity newUserFile = FileToEntityGenerator.generate(userId, fileName, fileBytes);

        Mockito.when(userFileRepository.findByUserId(Mockito.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(userFileRepository.insert(Mockito.any(UserFileEntity.class)))
                .thenReturn(newUserFile);

        UserFileEntity generatedUserFile = userFilePersistence.uploadFile(userId, fileName, fileBytes);

        assertThat(generatedUserFile.getUserId()).isEqualTo(newUserFile.getUserId());
        assertThat(generatedUserFile.getFileName()).isEqualTo(newUserFile.getFileName());
        assertThat(generatedUserFile.getFile()).isEqualTo(newUserFile.getFile());
    }
}
