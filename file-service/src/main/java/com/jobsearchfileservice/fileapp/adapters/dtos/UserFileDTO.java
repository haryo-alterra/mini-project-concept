package com.jobsearchfileservice.fileapp.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class UserFileDTO {
    @JsonProperty(required = true, value = "user_id")
    private String userId;

    @JsonProperty(value = "file_name")
    private String fileName;
}
