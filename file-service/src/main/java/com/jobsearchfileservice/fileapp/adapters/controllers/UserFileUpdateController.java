package com.jobsearchfileservice.fileapp.adapters.controllers;

import com.jobsearchfileservice.fileapp.adapters.dtos.ResponseDTO;
import com.jobsearchfileservice.fileapp.adapters.dtos.UserFileDTO;
import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileUpdateUseCase;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@RestController
@RequestMapping("/user_files")
@RequiredArgsConstructor
public class UserFileUpdateController {
    private final UserFileUpdateUseCase userFileUpdateUseCase;
    private final ModelMapper modelMapper;

    @PutMapping("/{userId}")
    public ResponseEntity<ResponseDTO> updateFile(
            @PathVariable String userId,
            @RequestParam("file_name") String fileName,
            @RequestParam("file") String base64FileString) {
        byte[] fileBytes = Base64.getDecoder().decode(base64FileString);
        UserFileEntity fileEntity = userFileUpdateUseCase.updateFile(
                userId, fileName, fileBytes
        );
        UserFileDTO fileRes = modelMapper.map(fileEntity, UserFileDTO.class);
        return new ResponseEntity<ResponseDTO>(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully updated user file")
                .data(fileRes).build(), HttpStatus.OK);
    }
}
