package com.jobsearchfileservice.fileapp.adapters.controllers;

import com.jobsearchfileservice.fileapp.adapters.dtos.ResponseDTO;
import com.jobsearchfileservice.fileapp.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileDownloadUseCase;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

@RestController
@RequestMapping("/user_files")
@RequiredArgsConstructor
public class UserFileDownloadController {
    private final UserFileDownloadUseCase userFileDownloadUseCase;

    @GetMapping("/{userId}")
    public ResponseEntity<ResponseDTO<UserFilePayloadDTO>> downloadFile(@PathVariable String userId) {
        UserFileEntity fileEntity = userFileDownloadUseCase.downloadFile(userId);
        UserFilePayloadDTO fileRes = new UserFilePayloadDTO();
        byte[] bytesArray = fileEntity.getFile().getData();

        fileRes.setBase64FileString(Base64.getEncoder().encodeToString(bytesArray));
        fileRes.setFileName(fileEntity.getFileName());
        fileRes.setUserId(fileEntity.getUserId());

        return new ResponseEntity<ResponseDTO<UserFilePayloadDTO>>(ResponseDTO
                .<UserFilePayloadDTO>builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved data")
                .data(fileRes).build(), HttpStatus.OK);
    }
}
