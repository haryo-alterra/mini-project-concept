package com.jobsearchfileservice.fileapp.adapters.entities;

import lombok.Getter;
import lombok.Setter;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "user_files")
public class UserFileEntity {
    @Id
    private String id;

    private String userId;

    private String fileName;

    private Binary file;

    public UserFileEntity(String userId, String fileName, Binary file) {
        this.userId = userId;
        this.fileName = fileName;
        this.file = file;
    }
}
