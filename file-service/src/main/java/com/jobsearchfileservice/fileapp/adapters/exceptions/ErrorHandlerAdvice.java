package com.jobsearchfileservice.fileapp.adapters.exceptions;

import com.jobsearchfileservice.fileapp.adapters.dtos.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandlerAdvice {
    @ExceptionHandler(value = UserFileAlreadyExistsException.class)
    public ResponseEntity<ResponseDTO> handlerFileAlreadyUploaded(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.CONFLICT)
                .message("Failed to upload file")
                .data(e.getMessage())
                .build(), HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(value = UserFileNotFoundException.class)
    public ResponseEntity<ResponseDTO> handlerFileNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.NOT_FOUND)
                .message("Failed to find relevant user file, either there is no file registered or given user id is invalid")
                .data(e.getMessage())
                .build(), HttpStatus.NOT_FOUND
        );
    }
}
