package com.jobsearchfileservice.fileapp.adapters.controllers;

import com.jobsearchfileservice.fileapp.adapters.dtos.ResponseDTO;
import com.jobsearchfileservice.fileapp.adapters.dtos.UserFileDTO;
import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileUploadUseCase;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

@RestController
@RequestMapping("/user_files")
@RequiredArgsConstructor
public class UserFileUploadController {
    private final UserFileUploadUseCase userFileUploadUseCase;
    private final ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<ResponseDTO> uploadFile(
        @RequestParam("user_id") String userId,
        @RequestParam("file_name") String fileName,
        @RequestParam("file") String base64FileString) {
        byte[] fileBytes = Base64.getDecoder().decode(base64FileString);
        UserFileEntity fileEntity = userFileUploadUseCase.uploadFile(
            userId, fileName, fileBytes
        );
        UserFileDTO fileRes = modelMapper.map(fileEntity, UserFileDTO.class);
        return new ResponseEntity<ResponseDTO>(ResponseDTO.builder()
            .httpStatus(HttpStatus.CREATED)
            .message("Successfully added user file")
            .data(fileRes).build(), HttpStatus.CREATED);
    }
}
