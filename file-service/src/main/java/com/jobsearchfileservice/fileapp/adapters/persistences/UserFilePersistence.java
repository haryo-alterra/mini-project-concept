package com.jobsearchfileservice.fileapp.adapters.persistences;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.adapters.exceptions.UserFileAlreadyExistsException;
import com.jobsearchfileservice.fileapp.adapters.exceptions.UserFileNotFoundException;
import com.jobsearchfileservice.fileapp.adapters.repositories.UserFileRepository;
import com.jobsearchfileservice.fileapp.application.out.UserFileDeletePort;
import com.jobsearchfileservice.fileapp.application.out.UserFileDownloadPort;
import com.jobsearchfileservice.fileapp.application.out.UserFileUpdatePort;
import com.jobsearchfileservice.fileapp.application.out.UserFileUploadPort;
import lombok.RequiredArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserFilePersistence implements
        UserFileUploadPort,
        UserFileDownloadPort,
        UserFileUpdatePort,
        UserFileDeletePort {
    private final UserFileRepository userFileRepository;

    @Override
    public final UserFileEntity uploadFile(String userId, String fileName, byte[] fileBytes) {
        Optional<UserFileEntity> foundUser = userFileRepository.findByUserId(userId);
        if (foundUser.isPresent()) {
            throw new UserFileAlreadyExistsException();
        }
        UserFileEntity newUserFile = new UserFileEntity(
            userId, fileName, new Binary(BsonBinarySubType.BINARY, fileBytes)
        );
        UserFileEntity insertedUserFile = userFileRepository.insert(newUserFile);
        return insertedUserFile;
    }

    @Override
    public final UserFileEntity downloadFile(String userId) {
        Optional<UserFileEntity> foundUser = userFileRepository.findByUserId(userId);
        if (foundUser.isEmpty()) {
            throw new UserFileNotFoundException();
        }
        return foundUser.get();
    }

    @Override
    public final UserFileEntity updateFile(String userId, String fileName, byte[] fileBytes) {
        Optional<UserFileEntity> targetUserFile = userFileRepository.findByUserId(userId);
        if (targetUserFile.isEmpty()) {
            throw new UserFileNotFoundException();
        }
        UserFileEntity fileEntity = targetUserFile.get();
        fileEntity.setFileName(fileName);
        fileEntity.setFile(new Binary(BsonBinarySubType.BINARY, fileBytes));
        UserFileEntity updatedUserFile = userFileRepository.save(fileEntity);
        return updatedUserFile;
    }

    @Override
    public final UserFileEntity deleteFile(String userId) {
        Optional<UserFileEntity> targetUserFile = userFileRepository.findByUserId(userId);
        if (targetUserFile.isEmpty()) {
            throw new UserFileNotFoundException();
        }
        UserFileEntity fileEntity = targetUserFile.get();
        userFileRepository.delete(fileEntity);
        return fileEntity;
    }
}
