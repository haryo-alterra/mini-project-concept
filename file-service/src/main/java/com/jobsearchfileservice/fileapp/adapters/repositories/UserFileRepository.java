package com.jobsearchfileservice.fileapp.adapters.repositories;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserFileRepository extends MongoRepository<UserFileEntity, String> {
    Optional<UserFileEntity> findByUserId(String userId);
}
