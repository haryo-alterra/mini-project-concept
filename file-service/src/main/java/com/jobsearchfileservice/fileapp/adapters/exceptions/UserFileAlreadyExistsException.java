package com.jobsearchfileservice.fileapp.adapters.exceptions;

import com.jobsearchfileservice.fileapp.adapters.exceptions.commons.DataExistException;

public class UserFileAlreadyExistsException extends DataExistException {
    public UserFileAlreadyExistsException() {
        super("User already uploaded a file");
    }
}
