package com.jobsearchfileservice.fileapp.adapters.exceptions;

import com.jobsearchfileservice.fileapp.adapters.exceptions.commons.DataNotExistException;

public class UserFileNotFoundException extends DataNotExistException {
    public UserFileNotFoundException() {
        super("User file not found in the database");
    }
}
