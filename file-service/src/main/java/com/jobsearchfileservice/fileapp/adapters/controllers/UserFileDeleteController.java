package com.jobsearchfileservice.fileapp.adapters.controllers;

import com.jobsearchfileservice.fileapp.adapters.dtos.ResponseDTO;
import com.jobsearchfileservice.fileapp.adapters.dtos.UserFileDTO;
import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileDeleteUseCase;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user_files")
@RequiredArgsConstructor
public class UserFileDeleteController {
    private final UserFileDeleteUseCase userFileDeleteUseCase;
    private final ModelMapper modelMapper;

    @DeleteMapping("/{userId}")
    public ResponseEntity<ResponseDTO> deleteFile(@PathVariable String userId) {
        UserFileEntity deletedEntity = userFileDeleteUseCase.deleteFile(userId);
        UserFileDTO fileRes = modelMapper.map(deletedEntity, UserFileDTO.class);
        return new ResponseEntity<ResponseDTO>(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully deleted user file")
                .data(fileRes).build(), HttpStatus.OK);
    }
}
