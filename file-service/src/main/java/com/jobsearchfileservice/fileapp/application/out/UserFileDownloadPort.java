package com.jobsearchfileservice.fileapp.application.out;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileDownloadPort {
    UserFileEntity downloadFile(String userId);
}
