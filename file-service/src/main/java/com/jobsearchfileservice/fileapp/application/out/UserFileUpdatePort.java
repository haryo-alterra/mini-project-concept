package com.jobsearchfileservice.fileapp.application.out;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileUpdatePort {
    UserFileEntity updateFile(String userId, String fileName, byte[] fileBytes);
}
