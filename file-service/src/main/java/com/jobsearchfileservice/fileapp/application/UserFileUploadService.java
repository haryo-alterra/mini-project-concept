package com.jobsearchfileservice.fileapp.application;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileUploadUseCase;
import com.jobsearchfileservice.fileapp.application.out.UserFileUploadPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileUploadService implements UserFileUploadUseCase {
    private final UserFileUploadPort userFileUploadPort;

    @Override
    public UserFileEntity uploadFile(String userId, String fileName, byte[] fileBytes) {
        return userFileUploadPort.uploadFile(userId, fileName, fileBytes);
    }
}
