package com.jobsearchfileservice.fileapp.application.in;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileDownloadUseCase {
    UserFileEntity downloadFile(String userId);
}
