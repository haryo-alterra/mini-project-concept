package com.jobsearchfileservice.fileapp.application.in;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileUploadUseCase {
    UserFileEntity uploadFile(String userId, String fileName, byte[] fileBytes);
}
