package com.jobsearchfileservice.fileapp.application;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileUpdateUseCase;
import com.jobsearchfileservice.fileapp.application.out.UserFileUpdatePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileUpdateService implements UserFileUpdateUseCase {
    private final UserFileUpdatePort userFileUpdatePort;

    @Override
    public UserFileEntity updateFile(String userId, String fileName, byte[] fileBytes) {
        return userFileUpdatePort.updateFile(userId, fileName, fileBytes);
    }
}
