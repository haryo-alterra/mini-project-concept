package com.jobsearchfileservice.fileapp.application;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileDeleteUseCase;
import com.jobsearchfileservice.fileapp.application.out.UserFileDeletePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileDeleteService implements UserFileDeleteUseCase {
    private final UserFileDeletePort userFileDeletePort;

    @Override
    public UserFileEntity deleteFile(String userId) {
        return userFileDeletePort.deleteFile(userId);
    }
}
