package com.jobsearchfileservice.fileapp.application.in;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileUpdateUseCase {
    UserFileEntity updateFile(String userId, String fileName, byte[] fileBytes);
}
