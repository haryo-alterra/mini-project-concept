package com.jobsearchfileservice.fileapp.application.in;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileDeleteUseCase {
    UserFileEntity deleteFile(String userId);
}
