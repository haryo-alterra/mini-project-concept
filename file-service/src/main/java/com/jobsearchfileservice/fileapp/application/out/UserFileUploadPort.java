package com.jobsearchfileservice.fileapp.application.out;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;

public interface UserFileUploadPort {
    UserFileEntity uploadFile(String userId, String fileName, byte[] fileBytes);
}
