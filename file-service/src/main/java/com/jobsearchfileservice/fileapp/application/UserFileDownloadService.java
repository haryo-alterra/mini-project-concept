package com.jobsearchfileservice.fileapp.application;

import com.jobsearchfileservice.fileapp.adapters.entities.UserFileEntity;
import com.jobsearchfileservice.fileapp.application.in.UserFileDownloadUseCase;
import com.jobsearchfileservice.fileapp.application.out.UserFileDownloadPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileDownloadService implements UserFileDownloadUseCase {
    private final UserFileDownloadPort userFileDownloadPort;

    @Override
    public UserFileEntity downloadFile(String userId) {
        return userFileDownloadPort.downloadFile(userId);
    }
}
