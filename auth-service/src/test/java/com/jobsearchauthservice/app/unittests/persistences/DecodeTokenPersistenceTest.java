package com.jobsearchauthservice.app.unittests.persistences;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.adapters.entities.RoleEntity;
import com.jobsearchauthservice.app.adapters.entities.UserEntity;
import com.jobsearchauthservice.app.adapters.exceptions.InvalidCredentialsException;
import com.jobsearchauthservice.app.adapters.persistences.AuthPersistence;
import com.jobsearchauthservice.app.adapters.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;

import java.time.Instant;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DecodeTokenPersistenceTest {
    @Mock
    UserRepository userRepository;

    @Mock
    JwtDecoder jwtDecoder;

    @InjectMocks
    AuthPersistence authPersistence;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given token does not represent any registered user, should return exception")
    public void givenInvalidToken_ShouldReturnException() {
        String tokenBearer = "Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoiYWJjZGUiLCJleHAiOjE2NzM4NDA4OTcsImlhdCI6MTY3MzgzNzI5Nywic2NvcGUiOiIifQ.cAjlTizd4VYvXaRmKa3hUV3nREnAV8ZaPmTyqBDtTq2mOr0aWMZmPu4Wz1YvM6ns86Qq-O2vbXWBC5NW6Vn_d2UjgppjljUVHNU8M5EO0FsBmGai3CChi0AKeM7ORMmuRJcCTndqTG3NBqJZ2JxFDhO22L_RklsRwQkH_TD0II9xpdx3p2xQAN079H1r1W8DCNEYZQcQYzuA2tQYeLVeJLbGsZ5AiV_NRNXggygm3HKZm1V0pPWGpdre9nRps_uEJEmoXgomhL_LIgI5ZBIet9fcFFFdBoJo1_njIww9uqL4__Qbq-UlCoIAGt7liAVyRSXuxk8KMEm0DIbzTL1Bug";
        String newToken = tokenBearer.split(" ")[1];
        HashMap<String, Object> jwtHeaders = new HashMap<String, Object>();
        HashMap<String, Object> jwtClaims = new HashMap<String, Object>();
        jwtHeaders.put("alg", "RS256");
        jwtClaims.put("sub", "kucingmeow1234");

        Jwt jwtToken = new Jwt(
                newToken,
                Instant.now(),
                Instant.now().plusSeconds(3600),
                jwtHeaders,
                jwtClaims
        );

        Mockito.when(jwtDecoder.decode(Mockito.anyString()))
                .thenReturn(jwtToken);
        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());
        assertThrows(InvalidCredentialsException.class, () ->
                authPersistence.decodeToken(tokenBearer));
    }

    @Test
    @DisplayName("Given valid token, should return referred basic user info")
    public void givenValidToken_ShouldReturnUserInfo() {
        String tokenBearer = "Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoiYWJjZGUiLCJleHAiOjE2NzM4NDA4OTcsImlhdCI6MTY3MzgzNzI5Nywic2NvcGUiOiIifQ.cAjlTizd4VYvXaRmKa3hUV3nREnAV8ZaPmTyqBDtTq2mOr0aWMZmPu4Wz1YvM6ns86Qq-O2vbXWBC5NW6Vn_d2UjgppjljUVHNU8M5EO0FsBmGai3CChi0AKeM7ORMmuRJcCTndqTG3NBqJZ2JxFDhO22L_RklsRwQkH_TD0II9xpdx3p2xQAN079H1r1W8DCNEYZQcQYzuA2tQYeLVeJLbGsZ5AiV_NRNXggygm3HKZm1V0pPWGpdre9nRps_uEJEmoXgomhL_LIgI5ZBIet9fcFFFdBoJo1_njIww9uqL4__Qbq-UlCoIAGt7liAVyRSXuxk8KMEm0DIbzTL1Bug";
        String newToken = tokenBearer.split(" ")[1];

        UserEntity testUser = new UserEntity();
        testUser.setId(UUID.fromString("0a09d130-b851-48a3-bb7f-323cc450fa99"));
        testUser.setUsername("kucingmeow1234");
        testUser.setEmail("kucingbisangoding@gmail.com");
        testUser.setPassword("$2a$12$lSUpA/ISa5uuECTmAiHFFu6qLBmmaaN7SuO2V7LjI5369Gk0xT6Q.");
        testUser.setName("Kucing");

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");
        testUser.setRole(role);

        HashMap<String, Object> jwtHeaders = new HashMap<String, Object>();
        HashMap<String, Object> jwtClaims = new HashMap<String, Object>();
        jwtHeaders.put("alg", "RS256");
        jwtClaims.put("sub", "kucingmeow1234");

        Jwt jwtToken = new Jwt(
                newToken,
                Instant.now(),
                Instant.now().plusSeconds(3600),
                jwtHeaders,
                jwtClaims
        );

        Mockito.when(jwtDecoder.decode(Mockito.anyString()))
                .thenReturn(jwtToken);
        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.of(testUser));
        UserDTO resultingUser = authPersistence.decodeToken(tokenBearer);

        assertThat(resultingUser.getId()).isEqualTo(testUser.getId());
        assertThat(resultingUser.getUsername()).isEqualTo(testUser.getUsername());
        assertThat(resultingUser.getPassword()).isEqualTo(testUser.getPassword());
        assertThat(resultingUser.getName()).isEqualTo(testUser.getName());
        assertThat(resultingUser.getRoleId()).isEqualTo(2);
    }
}
