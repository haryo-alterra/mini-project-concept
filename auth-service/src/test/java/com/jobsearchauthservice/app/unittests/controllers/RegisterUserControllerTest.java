package com.jobsearchauthservice.app.unittests.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchauthservice.app.adapters.controllers.RegisterUserController;
import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.RegisterUserUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RegisterUserController.class)
@AutoConfigureMockMvc(addFilters = false)
public class RegisterUserControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    RegisterUserUseCase registerUserUseCase;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("When register user, return response with http status 201")
    void whenSuccessfullyRegisterUser_thenReturns201() throws Exception {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        mockMvc.perform(post("/api/v1/auth/register")
            .content(objectMapper.writeValueAsString(user))
            .header("Content-Type", "application/json"))
            .andExpect(status().isCreated());

        then(registerUserUseCase).should()
            .registerUser(user);
    }
}
