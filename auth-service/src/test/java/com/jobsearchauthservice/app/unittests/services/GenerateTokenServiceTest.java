package com.jobsearchauthservice.app.unittests.services;

import com.jobsearchauthservice.app.application.GenerateTokenService;
import com.jobsearchauthservice.app.application.out.GenerateTokenPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GenerateTokenServiceTest {
    @Mock
    Authentication authentication;

    @Mock
    GenerateTokenPort generateTokenPort;

    @InjectMocks
    GenerateTokenService generateTokenService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid login, return JWT Token")
    public void givenValidLogin_thenReturnJWTToken() {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoiYWJjZGUiLCJleHAiOjE2NzM1MDIxOTksImlhdCI6MTY3MzQ5ODU5OSwic2NvcGUiOiIifQ.44AHjFS_gk2p2LDnuETgmdOHKn3HoDbCHE-AuU77HkjDFgs47wbeCoW4Bc0kk1T59NkM4qaxCYMEbSsIKDUxCvfVVQgs2LSwSBVzVs5yGyLwiwCKzxlqJB_SzTZzuHSBeeDV5JfxML_9vCKzTuQzG8U3jmnt4C52ENz-Cb2Ba8RAtk3YTlPmLyWfV_AAvdAQSsiZSgqRFVDAY15MqRhel4F9rMg2AgtIBEGJp3d5g21Y3rskrTOT2mNQhseFlVzUqFhO4VcCSn1MjPLUJWhCqWfoeGibTHhl7mBRNXpgM8z7pkcqk00biUYTgOiORZIuodqLYqyXM2YDQR22z7t7EA";
        Mockito.when(generateTokenPort.generateToken(authentication))
            .thenReturn(token);
        String generatedToken = generateTokenService.generateToken(authentication);
        assertThat(generatedToken).isEqualTo(token);
    }
}
