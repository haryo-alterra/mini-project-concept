package com.jobsearchauthservice.app.unittests.services;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.RegisterUserService;
import com.jobsearchauthservice.app.application.out.RegisterUserPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;

public class RegisterUserServiceTest {
    @Mock
    RegisterUserPort registerUserPort;


    @InjectMocks
    RegisterUserService registerUserService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a request to register user, should register it successfully")
    public void givenUserRegistration_ShouldExecuteSuccessfully() {
        Mockito.doNothing().when(registerUserPort).registerUser(Mockito.any(UserDTO.class));

        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        registerUserService.registerUser(user);
        Mockito.verify(registerUserPort).registerUser(user);
    }
}
