package com.jobsearchauthservice.app.unittests.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchauthservice.app.adapters.controllers.LoginController;
import com.jobsearchauthservice.app.adapters.entities.UserEntity;
import com.jobsearchauthservice.app.application.in.GenerateTokenUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = LoginController.class)
@AutoConfigureMockMvc(addFilters = false)
public class LoginControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Mock
    Authentication authentication;

    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    GenerateTokenUseCase generateTokenUseCase;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("When login with right credentials, return response with http status 200")
    void whenSuccessfullyLoggedIn_thenReturns200() throws Exception {
        UserEntity login = new UserEntity();
        login.setUsername("abcde");
        login.setPassword("abcABC123!@#");

        when(authenticationManager.authenticate(
                Mockito.any(UsernamePasswordAuthenticationToken.class)
        )).thenReturn(authentication);

        mockMvc.perform(post("/api/v1/auth/login")
            .content(objectMapper.writeValueAsString(login))
            .header("Content-Type", "application/json"))
            .andExpect(status().isOk());

        then(generateTokenUseCase)
            .should().generateToken(authentication);
    }
}
