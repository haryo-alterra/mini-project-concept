package com.jobsearchauthservice.app.unittests.services;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.DecodeTokenService;
import com.jobsearchauthservice.app.application.out.DecodeTokenPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class DecodeTokenServiceTest {
    @Mock
    DecodeTokenPort decodeTokenPort;

    @InjectMocks
    DecodeTokenService decodeTokenService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given valid token, should return user info successfully")
    public void givenValidToken_ShouldReturnUserInfo() {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoiYWJjZGUiLCJleHAiOjE2NzM4NDA4OTcsImlhdCI6MTY3MzgzNzI5Nywic2NvcGUiOiIifQ.cAjlTizd4VYvXaRmKa3hUV3nREnAV8ZaPmTyqBDtTq2mOr0aWMZmPu4Wz1YvM6ns86Qq-O2vbXWBC5NW6Vn_d2UjgppjljUVHNU8M5EO0FsBmGai3CChi0AKeM7ORMmuRJcCTndqTG3NBqJZ2JxFDhO22L_RklsRwQkH_TD0II9xpdx3p2xQAN079H1r1W8DCNEYZQcQYzuA2tQYeLVeJLbGsZ5AiV_NRNXggygm3HKZm1V0pPWGpdre9nRps_uEJEmoXgomhL_LIgI5ZBIet9fcFFFdBoJo1_njIww9uqL4__Qbq-UlCoIAGt7liAVyRSXuxk8KMEm0DIbzTL1Bug";
        UserDTO testUser = new UserDTO();
        testUser.setId(UUID.fromString("0a09d130-b851-48a3-bb7f-323cc450fa99"));
        testUser.setUsername("Meow123");
        testUser.setEmail("kucingbisangoding@gmail.com");
        testUser.setPassword("$2a$12$lSUpA/ISa5uuECTmAiHFFu6qLBmmaaN7SuO2V7LjI5369Gk0xT6Q.");
        testUser.setName("Kucing");
        testUser.setRoleId(2);

        Mockito.when(decodeTokenPort.decodeToken(Mockito.anyString()))
                .thenReturn(testUser);
        UserDTO generatedUser = decodeTokenService.decodeToken(token);
        assertThat(generatedUser.getId()).isEqualTo(testUser.getId());
        assertThat(generatedUser.getUsername()).isEqualTo(testUser.getUsername());
        assertThat(generatedUser.getPassword()).isEqualTo(testUser.getPassword());
        assertThat(generatedUser.getName()).isEqualTo(testUser.getName());
        assertThat(generatedUser.getRoleId()).isEqualTo(testUser.getRoleId());
    }
}
