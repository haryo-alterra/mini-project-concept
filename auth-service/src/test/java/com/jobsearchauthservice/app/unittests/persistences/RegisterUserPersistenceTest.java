package com.jobsearchauthservice.app.unittests.persistences;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.adapters.entities.RoleEntity;
import com.jobsearchauthservice.app.adapters.entities.UserEntity;
import com.jobsearchauthservice.app.adapters.exceptions.InvalidPasswordException;
import com.jobsearchauthservice.app.adapters.exceptions.RoleNotFoundException;
import com.jobsearchauthservice.app.adapters.exceptions.UserExistsException;
import com.jobsearchauthservice.app.adapters.persistences.AuthPersistence;
import com.jobsearchauthservice.app.adapters.repositories.RoleRepository;
import com.jobsearchauthservice.app.adapters.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RegisterUserPersistenceTest {
    @Mock
    UserRepository userRepository;

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    AuthPersistence authPersistence;

    @Spy
    ModelMapper modelMapper;

    @Spy
    PasswordEncoder passwordEncoder;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given already registered username, should return exception")
    public void givenUsernameAlreadyRegistered_ShouldReturnException() {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntity registeredUser = modelMapper.map(user, UserEntity.class);
        registeredUser.setId(UUID.randomUUID());
        registeredUser.setPassword(passwordEncoder.encode("abcABC123!@#"));

        Mockito.when(roleRepository.findById(Mockito.anyInt()))
            .thenReturn(Optional.of(role));
        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
            .thenReturn(Optional.of(registeredUser));
        assertThrows(UserExistsException.class, () ->
            authPersistence.registerUser(user));
    }

    @Test
    @DisplayName("Given already registered email, should return exception")
    public void givenEmailAlreadyRegistered_ShouldReturnException() {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntity registeredUser = modelMapper.map(user, UserEntity.class);
        registeredUser.setId(UUID.randomUUID());
        registeredUser.setPassword(passwordEncoder.encode("abcABC123!@#"));

        Mockito.when(roleRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(role));
        Mockito.when(userRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.of(registeredUser));
        assertThrows(UserExistsException.class, () ->
                authPersistence.registerUser(user));
    }

    @Test
    @DisplayName("Given role not found in repository, should return exception")
    public void givenRoleNotExisting_ShouldReturnException() {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        Mockito.when(roleRepository.findById(Mockito.anyInt()))
            .thenReturn(Optional.empty());
        assertThrows(RoleNotFoundException.class, () ->
            authPersistence.registerUser(user));
    }

    @Test
    @DisplayName("Given unsecure password, should return exception")
    public void givenUnsecurePassword_ShouldReturnException() {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("passwordletoy");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        Mockito.when(roleRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(role));
        Mockito.when(userRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());
        assertThrows(InvalidPasswordException.class, () ->
                authPersistence.registerUser(user));
    }

    @Test
    @DisplayName("Given successful registration, should save new user to user repository")
    public void givenSuccessfulRegistration_ShouldSaveToRepository() {
        UserDTO user = new UserDTO();
        user.setUsername("test");
        user.setPassword("abcABC123!@#");
        user.setEmail("test@gmail.com");
        user.setRoleId(1);
        user.setName("Test A");

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntity toBeSaved = modelMapper.map(user, UserEntity.class);
        toBeSaved.setPassword(passwordEncoder.encode("abcABC123!@#"));
        toBeSaved.setRole(role);

        Mockito.when(roleRepository.findById(Mockito.anyInt()))
                .thenReturn(Optional.of(role));
        Mockito.when(userRepository.findByEmail(Mockito.anyString()))
                .thenReturn(Optional.empty());
        Mockito.when(userRepository.findByUsername(Mockito.anyString()))
                .thenReturn(Optional.empty());
        authPersistence.registerUser(user);
        Mockito.verify(userRepository).save(toBeSaved);
    }
}
