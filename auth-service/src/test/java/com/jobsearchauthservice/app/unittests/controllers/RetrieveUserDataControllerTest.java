package com.jobsearchauthservice.app.unittests.controllers;

import com.jobsearchauthservice.app.adapters.controllers.RetrieveUserDataController;
import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.DecodeTokenUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = RetrieveUserDataController.class)
public class RetrieveUserDataControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    DecodeTokenUseCase decodeTokenUseCase;

    @Test
    @WithMockUser(username="admin",roles={"USER","ADMIN"})
    @DisplayName("When successfully get user info by login token, return 200")
    public void whenSuccessfullyGetUserInfoFromToken_ShouldReturn200() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJzZWxmIiwic3ViIjoiYWJjZGUiLCJleHAiOjE2NzM4NDA4OTcsImlhdCI6MTY3MzgzNzI5Nywic2NvcGUiOiIifQ.cAjlTizd4VYvXaRmKa3hUV3nREnAV8ZaPmTyqBDtTq2mOr0aWMZmPu4Wz1YvM6ns86Qq-O2vbXWBC5NW6Vn_d2UjgppjljUVHNU8M5EO0FsBmGai3CChi0AKeM7ORMmuRJcCTndqTG3NBqJZ2JxFDhO22L_RklsRwQkH_TD0II9xpdx3p2xQAN079H1r1W8DCNEYZQcQYzuA2tQYeLVeJLbGsZ5AiV_NRNXggygm3HKZm1V0pPWGpdre9nRps_uEJEmoXgomhL_LIgI5ZBIet9fcFFFdBoJo1_njIww9uqL4__Qbq-UlCoIAGt7liAVyRSXuxk8KMEm0DIbzTL1Bug";
        UserDTO testUser = new UserDTO();
        testUser.setId(UUID.fromString("0a09d130-b851-48a3-bb7f-323cc450fa99"));
        testUser.setUsername("Meow123");
        testUser.setEmail("kucingbisangoding@gmail.com");
        testUser.setPassword("$2a$12$lSUpA/ISa5uuECTmAiHFFu6qLBmmaaN7SuO2V7LjI5369Gk0xT6Q.");
        testUser.setName("Kucing");
        testUser.setRoleId(1);

        Mockito.when(decodeTokenUseCase.decodeToken(Mockito.anyString()))
            .thenReturn(testUser);
        mockMvc.perform(get("/api/v1/auth/user_data")
            .header("Authorization", "Bearer " + token))
            .andExpect(status().isOk());
    }
}
