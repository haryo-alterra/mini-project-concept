package com.jobsearchauthservice.app.application.in;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;

public interface DecodeTokenUseCase {
    UserDTO decodeToken(String token);
}
