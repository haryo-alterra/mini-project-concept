package com.jobsearchauthservice.app.application;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.DecodeTokenUseCase;
import com.jobsearchauthservice.app.application.out.DecodeTokenPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DecodeTokenService implements DecodeTokenUseCase {
    private final DecodeTokenPort decodeTokenPort;

    @Override
    public UserDTO decodeToken(String token) {
        return decodeTokenPort.decodeToken(token);
    }
}
