package com.jobsearchauthservice.app.application.in;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;

public interface RegisterUserUseCase {
    void registerUser(UserDTO user);
}
