package com.jobsearchauthservice.app.application;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.RegisterUserUseCase;
import com.jobsearchauthservice.app.application.out.RegisterUserPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegisterUserService implements RegisterUserUseCase {
    private final RegisterUserPort registerUserPort;

    @Override
    public void registerUser(UserDTO user) {
        registerUserPort.registerUser(user);
    }
}
