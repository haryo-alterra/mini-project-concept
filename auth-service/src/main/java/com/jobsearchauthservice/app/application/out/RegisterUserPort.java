package com.jobsearchauthservice.app.application.out;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;

public interface RegisterUserPort {
    void registerUser(UserDTO user);
}
