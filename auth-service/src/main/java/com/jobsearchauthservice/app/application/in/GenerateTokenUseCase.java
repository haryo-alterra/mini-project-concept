package com.jobsearchauthservice.app.application.in;

import org.springframework.security.core.Authentication;

public interface GenerateTokenUseCase {
    String generateToken(Authentication authentication);
}
