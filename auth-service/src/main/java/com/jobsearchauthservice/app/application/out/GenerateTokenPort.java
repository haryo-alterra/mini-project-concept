package com.jobsearchauthservice.app.application.out;

import org.springframework.security.core.Authentication;

public interface GenerateTokenPort {
    String generateToken(Authentication authentication);
}
