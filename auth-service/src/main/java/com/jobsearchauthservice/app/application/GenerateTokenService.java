package com.jobsearchauthservice.app.application;

import com.jobsearchauthservice.app.application.in.GenerateTokenUseCase;
import com.jobsearchauthservice.app.application.out.GenerateTokenPort;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GenerateTokenService implements GenerateTokenUseCase {
    private final GenerateTokenPort generateTokenPort;

    @Override
    public String generateToken(Authentication authentication) {
        return generateTokenPort.generateToken(authentication);
    }
}
