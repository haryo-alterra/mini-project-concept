package com.jobsearchauthservice.app.adapters.repositories;

import com.jobsearchauthservice.app.adapters.entities.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, String> {
    Optional<RoleEntity> findById(int id);
}
