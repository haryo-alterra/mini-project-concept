package com.jobsearchauthservice.app.adapters.exceptions;

import com.jobsearchauthservice.app.adapters.exceptions.commons.DataNotExistException;

public class InvalidCredentialsException extends DataNotExistException {
    public InvalidCredentialsException() {
        super("User not found, please check your login token");
    }
}
