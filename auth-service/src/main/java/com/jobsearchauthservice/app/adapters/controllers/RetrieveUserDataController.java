package com.jobsearchauthservice.app.adapters.controllers;

import com.jobsearchauthservice.app.adapters.dtos.ResponseDTO;
import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.DecodeTokenUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class RetrieveUserDataController {
    private final DecodeTokenUseCase decodeTokenUseCase;

    @GetMapping("/user_data")
    public ResponseEntity<ResponseDTO<Object>> getUserInfo(
            @RequestHeader(name = "Authorization") String token) {
        UserDTO user = decodeTokenUseCase.decodeToken(token);
        return new ResponseEntity(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("User with given token found")
                .data(user).build(), HttpStatus.OK);
    }
}
