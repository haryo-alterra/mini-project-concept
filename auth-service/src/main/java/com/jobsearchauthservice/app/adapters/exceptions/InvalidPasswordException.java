package com.jobsearchauthservice.app.adapters.exceptions;

public class InvalidPasswordException extends RuntimeException {
    public InvalidPasswordException() {
        super("Password must contain numbers (1234), capital letters (ABCD), and special characters (!@#$)");
    }
}
