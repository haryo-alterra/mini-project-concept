package com.jobsearchauthservice.app.adapters.controllers;

import com.jobsearchauthservice.app.adapters.dtos.ResponseDTO;
import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.application.in.RegisterUserUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class RegisterUserController {
    private final RegisterUserUseCase registerUserUseCase;

    @PostMapping("/register")
    public ResponseEntity<ResponseDTO> registerUser(@RequestBody UserDTO userDTO) {
        registerUserUseCase.registerUser(userDTO);
        return new ResponseEntity<ResponseDTO>(ResponseDTO.builder()
            .httpStatus(HttpStatus.CREATED)
            .message("Successfully registered user")
            .data(userDTO).build(), HttpStatus.CREATED);
    }
}
