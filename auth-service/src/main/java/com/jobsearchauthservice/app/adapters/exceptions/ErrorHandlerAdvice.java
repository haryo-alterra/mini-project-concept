package com.jobsearchauthservice.app.adapters.exceptions;

import com.jobsearchauthservice.app.adapters.dtos.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorHandlerAdvice {
    @ExceptionHandler(value = UserExistsException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerUserAlreadyRegistered(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
            .httpStatus(HttpStatus.CONFLICT)
            .message("Username/email already registered")
            .data(e.getMessage())
            .build(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = InvalidCredentialsException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerUserNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
            .httpStatus(HttpStatus.NOT_FOUND)
            .message("User not found")
            .data(e.getMessage())
            .build(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = RoleNotFoundException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerRoleNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
            .httpStatus(HttpStatus.NOT_FOUND)
            .message("Invalid role, role not found")
            .data(e.getMessage())
            .build(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InvalidPasswordException.class)
    public ResponseEntity<ResponseDTO<Object>> handlerInvalidPasswordInput(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
            .httpStatus(HttpStatus.BAD_REQUEST)
            .message("Password must contain numbers (1234), capital letters (ABCD), and special characters (!@#$)")
            .data(e.getMessage())
            .build(), HttpStatus.BAD_REQUEST);
    }
}
