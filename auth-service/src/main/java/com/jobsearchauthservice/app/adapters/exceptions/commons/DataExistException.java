package com.jobsearchauthservice.app.adapters.exceptions.commons;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.CONFLICT, reason = "Data already exists")
public class DataExistException extends RuntimeException {
    public DataExistException() {
        super();
    }

    public DataExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataExistException(String message) {
        super(message);
    }

    public DataExistException(Throwable cause) {
        super(cause);
    }
}
