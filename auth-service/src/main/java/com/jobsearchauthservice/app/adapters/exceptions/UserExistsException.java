package com.jobsearchauthservice.app.adapters.exceptions;

import com.jobsearchauthservice.app.adapters.exceptions.commons.DataExistException;

public class UserExistsException extends DataExistException {
    public UserExistsException() {
        super("Username / email already registered");
    }
}
