package com.jobsearchauthservice.app.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class UserDTO {
    @JsonDeserialize
    private UUID id;
    @JsonProperty(required = true)
    private String username;
    @JsonProperty(required = true)
    private String email;
    @JsonProperty(required = true)
    private String password;
    @JsonProperty(required = true)
    private String name;
    @JsonProperty("role_id")
    private int roleId = 2;
}
