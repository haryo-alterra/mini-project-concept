package com.jobsearchauthservice.app.adapters.persistences;

import com.jobsearchauthservice.app.adapters.dtos.UserDTO;
import com.jobsearchauthservice.app.adapters.entities.RoleEntity;
import com.jobsearchauthservice.app.adapters.entities.UserEntity;
import com.jobsearchauthservice.app.adapters.exceptions.InvalidPasswordException;
import com.jobsearchauthservice.app.adapters.exceptions.RoleNotFoundException;
import com.jobsearchauthservice.app.adapters.exceptions.UserExistsException;
import com.jobsearchauthservice.app.adapters.exceptions.InvalidCredentialsException;
import com.jobsearchauthservice.app.adapters.repositories.RoleRepository;
import com.jobsearchauthservice.app.adapters.repositories.UserRepository;
import com.jobsearchauthservice.app.application.out.DecodeTokenPort;
import com.jobsearchauthservice.app.application.out.GenerateTokenPort;
import com.jobsearchauthservice.app.application.out.RegisterUserPort;
import com.jobsearchauthservice.app.utils.PasswordValidator;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AuthPersistence implements
        GenerateTokenPort,
        DecodeTokenPort,
        RegisterUserPort {
    private final JwtEncoder jwtEncoder;
    private final JwtDecoder jwtDecoder;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public String generateToken(Authentication authentication) {
        Instant now = Instant.now();
        String scope = authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(""));
        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(now)
                .expiresAt(now.plus(1, ChronoUnit.HOURS))
                .subject(authentication.getName())
                .claim("scope", scope)
                .build();
        return this.jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    @Override
    public UserDTO decodeToken(String token) {
        String newToken = token.split(" ")[1];
        Jwt jwtToken = jwtDecoder.decode(newToken);
        String data = jwtToken.getSubject();
        Optional<UserEntity> user = userRepository.findByUsername(data);
        if (user.isPresent()) {
            return modelMapper.map(user.get(), UserDTO.class);
        }
        throw new InvalidCredentialsException();
    }

    @Override
    public void registerUser(UserDTO userDTO) {
        UserEntity user = modelMapper.map(userDTO, UserEntity.class);
        Optional<RoleEntity> role = roleRepository
                .findById(userDTO.getRoleId());
        Optional<UserEntity> userByUsername = userRepository
                .findByUsername(userDTO.getUsername());
        Optional<UserEntity> userByEmail = userRepository
                .findByEmail(userDTO.getEmail());

        if (userByUsername.isPresent() || userByEmail.isPresent()) {
            throw new UserExistsException();
        }
        if (role.isEmpty()) {
            throw new RoleNotFoundException();
        }
        if (!PasswordValidator.isValid(user.getPassword())) {
            throw new InvalidPasswordException();
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(role.get());
        userRepository.save(user);
    }
}
