package com.jobsearchauthservice.app.adapters.exceptions;

import com.jobsearchauthservice.app.adapters.exceptions.commons.DataNotExistException;

public class RoleNotFoundException extends DataNotExistException {
    public RoleNotFoundException() {
        super("Invalid role selected");
    }
}
