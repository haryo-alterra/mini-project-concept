package com.jobsearchauthservice.app.adapters.controllers;

import com.jobsearchauthservice.app.adapters.dtos.ResponseDTO;
import com.jobsearchauthservice.app.adapters.entities.UserEntity;
import com.jobsearchauthservice.app.application.in.GenerateTokenUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class LoginController {
    private final GenerateTokenUseCase generateTokenUseCase;

    private final AuthenticationManager authenticationManager;

    @PostMapping("/login")
    public ResponseEntity<ResponseDTO<String>> login(@RequestBody UserEntity user) {
        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                user.getUsername(),
                user.getPassword()
            )
        );
        String token = generateTokenUseCase.generateToken(authentication);
        return new ResponseEntity(
            ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Login token created")
                .data(token).build(),
                HttpStatus.OK
        );
    }
}
