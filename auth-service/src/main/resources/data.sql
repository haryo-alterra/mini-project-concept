insert into roles (id, name)
select 1, 'admin'
where not exists (
    select 1 from roles where name = 'admin'
);
insert into roles (id, name)
select 2, 'user'
where not exists (
    select 1 from roles where name = 'user'
);;