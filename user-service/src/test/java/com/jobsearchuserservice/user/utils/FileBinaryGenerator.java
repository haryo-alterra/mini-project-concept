package com.jobsearchuserservice.user.utils;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;

public class FileBinaryGenerator {
    public FileBinaryGenerator(){}
    public byte[] generate(String fileName) throws FileNotFoundException, IOException {
        File file = ResourceUtils.getFile(
                Objects.requireNonNull(this.getClass().getResource("/"+fileName)));
        FileInputStream fl = new FileInputStream(file);
        byte[] fileBytes  = new byte[(int)file.length()];
        fl.read(fileBytes);
        fl.close();
        return fileBytes;
    }
}
