package com.jobsearchuserservice.user.tests.persistences;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.adapters.persistences.UserFilePersistence;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Base64;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDownloadPersistenceTest {
    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    UserFilePersistence userFilePersistence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to download data via rest template exchange, should do it successfully")
    public void givenRequestToDownloadDataRestTemplateExchange_ShouldDoItSuccessfully() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFilePayloadDTO payload = new UserFilePayloadDTO();
        payload.setUserId(userId);
        payload.setFileName(fileName);
        payload.setBase64FileString(Base64.getEncoder().encodeToString(fileBytes));

        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved data")
                .data(payload).build(), HttpStatus.OK
        );

        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.GET),
                Mockito.eq(null),
                Mockito.eq(new ParameterizedTypeReference<ResponseDTO<UserFilePayloadDTO>>() {}))
        ).thenReturn(res);

        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> generatedResponse =
                userFilePersistence.downloadFile(userId);
        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully retrieved data");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(payload);
    }
}
