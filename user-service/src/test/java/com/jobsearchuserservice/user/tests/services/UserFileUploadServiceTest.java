package com.jobsearchuserservice.user.tests.services;

import com.jobsearchuserservice.user.application.UserFileUploadService;
import com.jobsearchuserservice.user.application.out.UserFileUploadPort;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileUploadServiceTest {
    @Mock
    UserFileUploadPort userFileUploadPort;

    @InjectMocks
    UserFileUploadService userFileUploadService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to upload data via rest template to file service, should do it successfully")
    public void givenRequestToUploadFileInRestTemplate_ShouldSuccessfullyDoIt() throws IOException {
        String serverUrl = "http://localhost:8083/user_files/";

        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);
        MockMultipartFile multipartFile = new MockMultipartFile("file", fileName, null, fileBytes);

        Mockito.when(userFileUploadPort.uploadFile(Mockito.anyString(), Mockito.any(MultipartFile.class)))
                .thenReturn(ResponseEntity.created(URI.create(serverUrl)).body("Success"));

        ResponseEntity<String> res = userFileUploadService.uploadFile(userId, multipartFile);
        assertThat(res.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
}
