package com.jobsearchuserservice.user.tests.services;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.UserFileUpdateService;
import com.jobsearchuserservice.user.application.out.UserFileUpdatePort;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileUpdateServiceTest {
    @Mock
    UserFileUpdatePort userFileUpdatePort;

    @InjectMocks
    UserFileUpdateService userFileUpdateService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to update data via rest template to file service, should do it successfully")
    public void givenRequestToUpdateFileInRestTemplate_ShouldSuccessfullyDoIt() throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);
        MockMultipartFile multipartFile = new MockMultipartFile("file", fileName, null, fileBytes);

        ResponseEntity<ResponseDTO<UserFileDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully updated user file")
                .data(userFileInfo).build(), HttpStatus.OK
        );

        Mockito.when(userFileUpdatePort.updateFile(Mockito.anyString(), Mockito.any(MultipartFile.class)))
                .thenReturn(res);

        ResponseEntity<ResponseDTO<UserFileDTO>> generatedResponse
                = userFileUpdateService.updateFile(userId, multipartFile);
        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully updated user file");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(userFileInfo);
    }
}
