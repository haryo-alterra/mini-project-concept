package com.jobsearchuserservice.user.tests.controllers;

import com.jobsearchuserservice.user.adapters.controllers.UserProfileRetrievalController;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.adapters.entities.RoleEntity;
import com.jobsearchuserservice.user.adapters.entities.UserEntity;
import com.jobsearchuserservice.user.application.in.UserProfileRetrievalUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserProfileRetrievalController.class)
public class UserProfileRetrievalControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserProfileRetrievalUseCase userProfileRetrievalUseCase;

    @Spy
    ModelMapper modelMapper;

    @Test
    @DisplayName("Given request to retrieve user profile, should return 200")
    public void givenRequestToRetrieveProfile_ShouldReturn200() throws Exception {
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";

        mockMvc.perform(get("/api/v1/profile")
                .header("user_id", userId))
                .andExpect(status().isOk());

        then(userProfileRetrievalUseCase)
                .should().getUserProfileById(userId);
    }

    @Test
    @DisplayName("Given request to retrieve user profile by user ID, should return 200")
    public void givenRequestToRetrieveAllProfile_ShouldReturn200() throws Exception {
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String searchedId = "e1855af4-0241-459d-ac7c-686051fc2d30";

        LocalDateTime datetime = LocalDateTime.now();

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntity entity = UserEntity.builder()
                .id(UUID.fromString(searchedId))
                .username("abcdefghijkl")
                .email("a.gmail.com")
                .password("$2a$10$S0ze4gymAylf3hKze0MbSuAaY.ohGbgjaERGjC30HrfPQtnSimsQ2")
                .name("A")
                .role(role)
                .createdAt(datetime)
                .updatedAt(datetime).build();
        UserEntityDTO userData = modelMapper.map(entity, UserEntityDTO.class);

        Mockito.when(userProfileRetrievalUseCase.getUserProfileById(Mockito.anyString()))
                .thenReturn(userData);
        mockMvc.perform(get("/api/v1/user/"+searchedId)
                .header("user_id", userId))
                .andExpect(status().isOk());

        then(userProfileRetrievalUseCase)
                .should().getUserProfileById(searchedId);
    }
}
