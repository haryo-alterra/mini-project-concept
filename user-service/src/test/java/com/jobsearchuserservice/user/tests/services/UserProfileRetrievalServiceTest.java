package com.jobsearchuserservice.user.tests.services;

import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.adapters.entities.RoleEntity;
import com.jobsearchuserservice.user.application.UserProfileRetrievalService;
import com.jobsearchuserservice.user.application.out.UserProfileRetrievalPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserProfileRetrievalServiceTest {
    @Mock
    UserProfileRetrievalPort userProfileRetrievalPort;

    @InjectMocks
    UserProfileRetrievalService userProfileRetrievalService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given a registered user ID in UUID format, should return the referred user data")
    public void givenRegisteredUserUUID_ShouldReturnProfileData() {
        String userId = "95883420-a79a-4a49-b9e9-82c0df311dfc";

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntityDTO userData = new UserEntityDTO();
        userData.setEmail("a@gmail.com");
        userData.setId(UUID.fromString(userId));
        userData.setName("A");
        userData.setUsername("abcdefghijkl");
        userData.setRole(role);

        Mockito.when(userProfileRetrievalPort.getUserProfileById(Mockito.anyString()))
                .thenReturn(userData);
        UserEntityDTO generatedUserData
                = userProfileRetrievalService.getUserProfileById(userId);

        assertThat(generatedUserData.getId()).isEqualTo(userData.getId());
        assertThat(generatedUserData.getEmail()).isEqualTo(userData.getEmail());
        assertThat(generatedUserData.getName()).isEqualTo(userData.getName());
        assertThat(generatedUserData.getUsername()).isEqualTo(userData.getUsername());
        assertThat(generatedUserData.getRole()).isEqualTo(userData.getRole());
    }
}
