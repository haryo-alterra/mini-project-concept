package com.jobsearchuserservice.user.tests.persistences;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.adapters.persistences.UserFilePersistence;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDeletePersistenceTest {
    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    UserFilePersistence userFilePersistence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to delete saved data, should do it successfully")
    public void givenRequestToDeleteDataRestTemplateExchange_ShouldDoItSuccessfully() throws Exception {
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);

        ResponseEntity<ResponseDTO<UserFileDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully deleted user file")
                .data(userFileInfo).build(), HttpStatus.OK
        );

        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.DELETE),
                Mockito.eq(null),
                Mockito.eq(new ParameterizedTypeReference<ResponseDTO<UserFileDTO>>() {}))
        ).thenReturn(res);

        ResponseEntity<ResponseDTO<UserFileDTO>> generatedResponse =
                userFilePersistence.deleteFile(userId);
        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully deleted user file");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(userFileInfo);
    }
}
