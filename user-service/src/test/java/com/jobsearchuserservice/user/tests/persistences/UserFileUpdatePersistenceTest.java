package com.jobsearchuserservice.user.tests.persistences;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.persistences.UserFilePersistence;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Base64;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

// TODO: Blocking issue - Fix RestTemplate Testing (CHANGE ResponseDTO)
public class UserFileUpdatePersistenceTest {
    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    UserFilePersistence userFilePersistence;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to update data via rest template exchange, should do it successfully")
    public void givenRequestToUpdateDataRestTemplateExchange_ShouldDoItSuccessfully() throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);
        MockMultipartFile multipartFile = new MockMultipartFile("file", fileName, null, fileBytes);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("user_id", userId);
        body.add("file_name", fileName);
        body.add("file", Base64.getEncoder().encodeToString(fileBytes));

        ResponseEntity<ResponseDTO<UserFileDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully updated user file")
                .data(userFileInfo).build(), HttpStatus.OK
        );

        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.PUT),
                Mockito.any(HttpEntity.class),
                Mockito.eq(new ParameterizedTypeReference<ResponseDTO<UserFileDTO>>() {}))
        ).thenReturn(res);

        ResponseEntity<ResponseDTO<UserFileDTO>> generatedResponse
                = userFilePersistence.updateFile(userId, multipartFile);
        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully updated user file");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(userFileInfo);
    }

}
