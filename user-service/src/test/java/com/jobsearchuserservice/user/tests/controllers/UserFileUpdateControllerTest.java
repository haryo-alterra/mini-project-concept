package com.jobsearchuserservice.user.tests.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobsearchuserservice.user.adapters.controllers.UserFileUpdateController;
import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileUpdateUseCase;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileUpdateController.class)
public class UserFileUpdateControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileUpdateUseCase userFileUpdateUseCase;

    @Test
    @DisplayName("Given request to update file, should return 200")
    public void givenRequestToUpdateFile_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);
        MockMultipartFile multipartFile = new MockMultipartFile(
                "file", fileName, null, fileBytes);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/api/v1/profile/file");

        builder.with(request -> {
            request.setContentType("multipart/form-data");
            request.setMethod("PUT");
            request.setParameter("file",
                    Base64.getEncoder().encodeToString(fileBytes));
            return request;
        });

        HttpHeaders headers = new HttpHeaders();
        headers.set("user_id", userId);

        mockMvc.perform(builder.file(multipartFile)
                .headers(headers))
                .andExpect(status().isOk());
        then(userFileUpdateUseCase).should()
                .updateFile(userId, multipartFile);
    }
}
