package com.jobsearchuserservice.user.tests.controllers;

import com.jobsearchuserservice.user.adapters.controllers.UserProfileAllRetrievalController;
import com.jobsearchuserservice.user.application.in.UserProfileRetrievalUseCase;
import com.jobsearchuserservice.user.application.in.UserProfileRetrieveAllUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserProfileAllRetrievalController.class)
public class UserProfileRetrievalAllControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserProfileRetrieveAllUseCase userProfileRetrievalAllUseCase;

    @Test
    @DisplayName("Given request to retrieve ALL user profile, should return 200")
    public void givenRequestToRetrieveAllProfile_ShouldReturn200() throws Exception {
        mockMvc.perform(get("/api/v1/user/"))
                .andExpect(status().isOk());

        then(userProfileRetrievalAllUseCase)
                .should().getAllUsers(Mockito.any(Pageable.class));
    }
}
