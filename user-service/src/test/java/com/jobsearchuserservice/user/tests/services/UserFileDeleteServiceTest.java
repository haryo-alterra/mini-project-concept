package com.jobsearchuserservice.user.tests.services;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.UserFileDeleteService;
import com.jobsearchuserservice.user.application.out.UserFileDeletePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDeleteServiceTest {
    @Mock
    UserFileDeletePort userFileDeletePort;

    @InjectMocks
    UserFileDeleteService userFileDeleteService;

    @Mock
    ResponseEntity<ResponseDTO<UserFileDTO>> response;

    @Mock
    ResponseDTO<UserFileDTO> mockResponseDTO;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to delete data via rest template to file service, should do it successfully")
    public void givenRequestToDeleteFileInRestTemplate_ShouldSuccessfullyDoIt() {
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);

        ResponseEntity<ResponseDTO<UserFileDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully deleted user file")
                .data(userFileInfo).build(), HttpStatus.OK
        );

        Mockito.when(userFileDeletePort.deleteFile(Mockito.anyString()))
                .thenReturn(res);

        ResponseEntity<ResponseDTO<UserFileDTO>> generatedResponse = userFileDeleteService.deleteFile(userId);
        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully deleted user file");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(userFileInfo);
    }
}
