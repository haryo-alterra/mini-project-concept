package com.jobsearchuserservice.user.tests.persistences;

import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.adapters.entities.RoleEntity;
import com.jobsearchuserservice.user.adapters.entities.UserEntity;
import com.jobsearchuserservice.user.adapters.exceptions.UserNotFoundException;
import com.jobsearchuserservice.user.adapters.persistences.UserDetailsPersistence;
import com.jobsearchuserservice.user.adapters.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UserProfileRetrievalPersistenceTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserDetailsPersistence userDetailsPersistence;

    @Spy
    ModelMapper modelMapper;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given invalid user uuid, should return exception")
    public void givenInvalidUserUUID_ShouldReturnException() {
        String userId = "95883420-a79a-4a49-b9e9-82c0df311dfc";
        Mockito.when(userRepository.findByUUID(Mockito.any(UUID.class)))
                .thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () ->
                userDetailsPersistence.getUserProfileById(userId));
    }

    @Test
    @DisplayName("Given request to get user profile, should do it successfully")
    public void givenRequestToGetUserProfile_ShouldDoItSuccessfully() {
        String userId = "95883420-a79a-4a49-b9e9-82c0df311dfc";
        LocalDateTime datetime = LocalDateTime.now();

        RoleEntity role = new RoleEntity();
        role.setId(2);
        role.setName("user");

        UserEntity entity = UserEntity.builder()
                .id(UUID.fromString(userId))
                .username("abcdefghijkl")
                .email("a.gmail.com")
                .password("$2a$10$S0ze4gymAylf3hKze0MbSuAaY.ohGbgjaERGjC30HrfPQtnSimsQ2")
                .name("A")
                .role(role)
                .createdAt(datetime)
                .updatedAt(datetime).build();
        UserEntityDTO userData = modelMapper.map(entity, UserEntityDTO.class);

        Mockito.when(userRepository.findByUUID(Mockito.any(UUID.class)))
                .thenReturn(Optional.of(entity));

        UserEntityDTO generatedUserData = userDetailsPersistence.getUserProfileById(userId);
        assertThat(generatedUserData.getId()).isEqualTo(userData.getId());
        assertThat(generatedUserData.getEmail()).isEqualTo(userData.getEmail());
        assertThat(generatedUserData.getName()).isEqualTo(userData.getName());
        assertThat(generatedUserData.getUsername()).isEqualTo(userData.getUsername());
        assertThat(generatedUserData.getRole()).isEqualTo(userData.getRole());
    }
}
