package com.jobsearchuserservice.user.tests.controllers;

import com.jobsearchuserservice.user.adapters.controllers.UserFileDownloadController;
import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.in.UserFileDownloadUseCase;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Base64;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileDownloadController.class)
@AutoConfigureMockMvc(addFilters = false)
public class UserFileDownloadControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileDownloadUseCase userFileDownloadUseCase;

    @MockBean
    ModelMapper modelMapper;

    @Mock
    ResponseEntity<ResponseDTO<UserFilePayloadDTO>> response;

    @Mock
    ResponseDTO<UserFilePayloadDTO> mockResponseDTO;

    @Test
    @DisplayName("Given request to download file, should return 200")
    public void givenRequestToDownloadFile_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFilePayloadDTO payload = new UserFilePayloadDTO();
        payload.setUserId(userId);
        payload.setFileName(fileName);
        payload.setBase64FileString(Base64.getEncoder().encodeToString(fileBytes));

        Mockito.when(response.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.when(response.getBody()).thenReturn(mockResponseDTO);
        Mockito.when(mockResponseDTO.getMessage()).thenReturn("Success");
        Mockito.when(mockResponseDTO.getData()).thenReturn(payload);

        Mockito.when(userFileDownloadUseCase.downloadFile(Mockito.anyString()))
                .thenReturn(response);
        mockMvc.perform(get("/api/v1/profile/file")
                .header("user_id", userId))
                .andExpect(status().isOk());

        then(userFileDownloadUseCase).should()
                .downloadFile(userId);
    }

    @Test
    @DisplayName("Given request to download file by ID, should return 200")
    public void givenRequestToDownloadFileById_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFilePayloadDTO payload = new UserFilePayloadDTO();
        payload.setUserId(userId);
        payload.setFileName(fileName);
        payload.setBase64FileString(Base64.getEncoder().encodeToString(fileBytes));

        Mockito.when(response.getStatusCode()).thenReturn(HttpStatus.OK);
        Mockito.when(response.getBody()).thenReturn(mockResponseDTO);
        Mockito.when(mockResponseDTO.getMessage()).thenReturn("Success");
        Mockito.when(mockResponseDTO.getData()).thenReturn(payload);

        Mockito.when(userFileDownloadUseCase.downloadFile(Mockito.anyString()))
                .thenReturn(response);
        mockMvc.perform(get("/api/v1/user/"+userId+"/file"))
                .andExpect(status().isOk());

        then(userFileDownloadUseCase).should()
                .downloadFile(userId);
    }
}
