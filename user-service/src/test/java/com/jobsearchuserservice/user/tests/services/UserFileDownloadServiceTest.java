package com.jobsearchuserservice.user.tests.services;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.UserFileDownloadService;
import com.jobsearchuserservice.user.application.out.UserFileDownloadPort;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Base64;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class UserFileDownloadServiceTest {
    @Mock
    UserFileDownloadPort userFileDownloadPort;

    @InjectMocks
    UserFileDownloadService userFileDownloadService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Given request to download data via rest template to file service, should do it successfully")
    public void givenRequestToDownloadFileInRestTemplate_ShouldSuccessfullyDoIt() throws IOException {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFilePayloadDTO payload = new UserFilePayloadDTO();
        payload.setUserId(userId);
        payload.setFileName(fileName);
        payload.setBase64FileString(Base64.getEncoder().encodeToString(fileBytes));

        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> res = new ResponseEntity(ResponseDTO
                .builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved data")
                .data(payload).build(), HttpStatus.OK
        );

        Mockito.when(userFileDownloadPort.downloadFile(Mockito.anyString()))
                .thenReturn(res);
        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> generatedResponse =
                userFileDownloadService.downloadFile(userId);

        assertThat(generatedResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(generatedResponse.getBody().getMessage()).isEqualTo("Successfully retrieved data");
        assertThat(generatedResponse.getBody().getData()).isEqualTo(payload);
    }
}
