package com.jobsearchuserservice.user.tests.controllers;

import com.jobsearchuserservice.user.adapters.controllers.UserFileDeleteController;
import com.jobsearchuserservice.user.application.in.UserFileDeleteUseCase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileDeleteController.class)
public class UserFileDeleteControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileDeleteUseCase userFileDeleteUseCase;

    @Test
    @DisplayName("Given request to delete file, should return 200")
    public void givenRequestToDeleteFile_ShouldReturn200() throws Exception {
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";

        mockMvc.perform(delete("/api/v1/profile/file")
                .header("user_id", userId))
                .andExpect(status().isOk());

        then(userFileDeleteUseCase).should()
                .deleteFile(userId);
    }
}
