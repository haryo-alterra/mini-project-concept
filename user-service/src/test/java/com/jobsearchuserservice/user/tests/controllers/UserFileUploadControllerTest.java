package com.jobsearchuserservice.user.tests.controllers;

import com.jobsearchuserservice.user.adapters.controllers.UserFileUploadController;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileUploadUseCase;
import com.jobsearchuserservice.user.utils.FileBinaryGenerator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Base64;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserFileUploadController.class)
public class UserFileUploadControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserFileUploadUseCase userFileUploadUseCase;

    @Test
    @DisplayName("Given request to upload file, should return 200")
    public void givenRequestToUploadFile_ShouldReturn200() throws Exception {
        FileBinaryGenerator generator = new FileBinaryGenerator();
        String userId = "9f966e77-8367-41e7-9a8a-0271a3670f5a";
        String fileName = "dummy_cv.docx";
        byte[] fileBytes = generator.generate(fileName);

        UserFileDTO userFileInfo = new UserFileDTO();
        userFileInfo.setFileName(fileName);
        userFileInfo.setUserId(userId);
        MockMultipartFile multipartFile = new MockMultipartFile(
                "file", fileName, null, fileBytes);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/api/v1/profile/file");

        builder.with(request -> {
            request.setContentType("multipart/form-data");
            request.setMethod("POST");
            request.setParameter("file",
                    Base64.getEncoder().encodeToString(fileBytes));
            return request;
        });

        HttpHeaders headers = new HttpHeaders();
        headers.set("user_id", userId);

        mockMvc.perform(builder.file(multipartFile)
                .headers(headers))
                .andExpect(status().isOk());
        then(userFileUploadUseCase).should()
                .uploadFile(userId, multipartFile);
    }
}
