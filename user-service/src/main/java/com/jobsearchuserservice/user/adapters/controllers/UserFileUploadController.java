package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.application.in.UserFileUploadUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserFileUploadController {
    private final UserFileUploadUseCase userFileUploadUseCase;

    @PostMapping("/profile/file")
    public ResponseEntity<String> uploadFile(
            @RequestHeader(name = "user_id") String userId,
            @RequestParam("file") MultipartFile file) throws IOException {
        return userFileUploadUseCase.uploadFile(userId, file);
    }
}
