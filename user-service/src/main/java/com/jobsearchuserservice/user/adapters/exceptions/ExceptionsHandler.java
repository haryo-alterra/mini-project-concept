package com.jobsearchuserservice.user.adapters.exceptions;

import com.google.gson.Gson;
import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;

@RestControllerAdvice
public class ExceptionsHandler {
    @ExceptionHandler(value = HttpClientErrorException.class)
    public ResponseEntity<ResponseDTO> handlerHttpClientError(
            HttpClientErrorException e) {
        Gson gson = new Gson();
        return new ResponseEntity<>(gson.fromJson(
                e.getResponseBodyAsString(), ResponseDTO.class), e.getStatusCode());
    }

    @ExceptionHandler(value = IOException.class)
    public ResponseEntity<ResponseDTO> handlerIOError(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Server data handling failure, please contact the admin")
                .data(e.getMessage())
                .build(), HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<ResponseDTO> handlerUserNotFound(Exception e) {
        return new ResponseEntity<>(ResponseDTO.builder()
                .httpStatus(HttpStatus.NOT_FOUND)
                .message("User not found.")
                .data(e.getMessage())
                .build(), HttpStatus.NOT_FOUND
        );
    }
}
