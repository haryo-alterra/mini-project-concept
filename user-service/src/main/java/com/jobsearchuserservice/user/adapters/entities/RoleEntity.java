package com.jobsearchuserservice.user.adapters.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "roles")
public class RoleEntity implements Serializable {
    @Id
    @JsonProperty(value = "role_id")
    private Integer id;

    @Column(nullable = false)
    @JsonProperty(value = "role_name")
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "role")
    private List<UserEntity> users;
}
