package com.jobsearchuserservice.user.adapters.persistences;

import com.jobsearchuserservice.user.adapters.dtos.NameChangeDTO;
import com.jobsearchuserservice.user.adapters.dtos.PaginationUserEntityDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.adapters.entities.UserEntity;
import com.jobsearchuserservice.user.adapters.exceptions.UserNotFoundException;
import com.jobsearchuserservice.user.adapters.repositories.UserRepository;
import com.jobsearchuserservice.user.application.out.UserEditNamePort;
import com.jobsearchuserservice.user.application.out.UserProfileRetrievalPort;
import com.jobsearchuserservice.user.application.out.UserProfileRetrieveAllPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
@Log4j2
public class UserDetailsPersistence implements
        UserProfileRetrievalPort,
        UserProfileRetrieveAllPort,
        UserEditNamePort {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public UserEntityDTO getUserProfileById(String userId) {
        UUID uuidUser = UUID.fromString(userId);
        Optional<UserEntity> referredUser = userRepository.findByUUID(uuidUser);
        if (referredUser.isEmpty()) {
            throw new UserNotFoundException();
        }
        UserEntity user = referredUser.get();
        log.info("Name: "+ user.getName());
        log.info("Email: "+ user.getEmail());
        return modelMapper.map(user, UserEntityDTO.class);
    }

    @Override
    public PaginationUserEntityDTO getAllUsers(Pageable pageable) {
        List<UserEntityDTO> userDTOList = new ArrayList<>();
        Page<UserEntity> allUsers = userRepository.findAll(pageable);
        for (UserEntity entity: allUsers.getContent()) {
            userDTOList.add(modelMapper.map(entity, UserEntityDTO.class));
        }
        return PaginationUserEntityDTO.builder()
                .userDTOList(userDTOList)
                .totalOfItems(allUsers.getTotalElements())
                .totalOfPages(allUsers.getTotalPages())
                .currentPage(allUsers.getNumber())
                .build();
    }

    @Override
    public UserEntityDTO editName(String userId, NameChangeDTO newName) {
        UUID uuidUser = UUID.fromString(userId);
        Optional<UserEntity> referredUser = userRepository.findByUUID(uuidUser);
        if (referredUser.isEmpty()) {
            throw new UserNotFoundException();
        }
        UserEntity user = referredUser.get();
        user.setName(newName.getName());
        UserEntity updatedUser = userRepository.save(user);
        return modelMapper.map(updatedUser, UserEntityDTO.class);
    }
}
