package com.jobsearchuserservice.user.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class UserFilePayloadDTO {
    @JsonProperty(required = true, value = "user_id")
    private String userId;

    @JsonProperty(required = true, value = "file_name")
    private String fileName;

    @JsonProperty(required = true, value = "file")
    private String base64FileString;
}
