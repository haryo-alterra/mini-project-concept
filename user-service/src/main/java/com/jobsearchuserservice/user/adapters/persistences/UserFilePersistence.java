package com.jobsearchuserservice.user.adapters.persistences;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.out.UserFileDeletePort;
import com.jobsearchuserservice.user.application.out.UserFileDownloadPort;
import com.jobsearchuserservice.user.application.out.UserFileUpdatePort;
import com.jobsearchuserservice.user.application.out.UserFileUploadPort;
import com.jobsearchuserservice.user.utils.FileRequestGeneration;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class UserFilePersistence implements
        UserFileUploadPort,
        UserFileDownloadPort,
        UserFileUpdatePort,
        UserFileDeletePort {
    @Value("${file.uri}")
    private String serverUrl;
    private final RestTemplate restTemplate;

    @Override
    public ResponseEntity<String> uploadFile(String userId, MultipartFile file) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
                FileRequestGeneration.generate(userId, file), headers);
        return restTemplate
                .postForEntity(serverUrl, requestEntity, String.class);
    }

    @Override
    public ResponseEntity<ResponseDTO<UserFilePayloadDTO>> downloadFile(String userId) {
        return restTemplate.exchange(
                serverUrl+userId, HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {});
    }

    @Override
    public ResponseEntity<ResponseDTO<UserFileDTO>> updateFile(String userId, MultipartFile file)
            throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
                FileRequestGeneration.generate(userId, file), headers);
        return restTemplate.exchange(
                serverUrl+userId, HttpMethod.PUT, requestEntity,
                new ParameterizedTypeReference<>() {});
    }

    @Override
    public ResponseEntity<ResponseDTO<UserFileDTO>> deleteFile(String userId) {
        return restTemplate.exchange(
                serverUrl+userId, HttpMethod.DELETE, null,
                new ParameterizedTypeReference<>() {}
        );
    }
}