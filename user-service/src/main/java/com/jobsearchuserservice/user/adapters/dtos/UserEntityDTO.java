package com.jobsearchuserservice.user.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jobsearchuserservice.user.adapters.entities.RoleEntity;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class UserEntityDTO implements Serializable {
    @JsonProperty(value = "user_id")
    private UUID id;

    private String username;

    private String email;

    private String name;

    private RoleEntity role;

    @JsonProperty(value = "created_at")
    private LocalDateTime createdAt;
}
