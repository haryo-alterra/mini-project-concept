package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileUpdateUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserFileUpdateController {
    private final UserFileUpdateUseCase userFileUpdateUseCase;

    @PutMapping("/profile/file")
    public ResponseEntity<ResponseDTO<UserFileDTO>> updateFile(
            @RequestHeader(name = "user_id") String userId,
            @RequestParam("file") MultipartFile file) throws IOException {
        return userFileUpdateUseCase.updateFile(userId, file);
    }
}
