package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.application.in.UserProfileRetrievalUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserProfileRetrievalController {
    private final UserProfileRetrievalUseCase userProfileRetrievalUseCase;

    @GetMapping("/profile")
    public ResponseEntity<ResponseDTO<UserEntityDTO>> retrieveProfile(
            @RequestHeader(name = "user_id") String userId) {
        UserEntityDTO userProfile
                = userProfileRetrievalUseCase.getUserProfileById(userId);
        return new ResponseEntity(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Profile retrieved")
                .data(userProfile).build(), HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<ResponseDTO<UserEntityDTO>> retrieveProfileById(
            @PathVariable String userId) {
        UserEntityDTO userProfile
                = userProfileRetrievalUseCase.getUserProfileById(userId);
        return new ResponseEntity(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Profile with ID " + userId + " retrieved.")
                .data(userProfile).build(), HttpStatus.OK);
    }
}
