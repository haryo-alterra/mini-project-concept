package com.jobsearchuserservice.user.adapters.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="user_details")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserDetailsEntity implements Serializable {

    @Id
    @Column(name = "user_id")
    private UUID id;

    private String description;

    private String address;

    private Character gender;

    @Column(name = "place_of_birth")
    private String pob;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_birth")
    private Date dob;

    @Column(name = "years_of_experience")
    private Integer yoe;

    @OneToOne
    @MapsId
    @JoinColumn(name = "user_id")
    private UserEntity user;
}
