package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.in.UserFileDownloadUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Base64;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserFileDownloadController {
    private final UserFileDownloadUseCase userFileDownloadUseCase;

    public void streamDownloadedFile(
            HttpServletResponse response, UserFilePayloadDTO payload) throws IOException {
        response.setContentType("application/octet-stream");
        response.setHeader(
                HttpHeaders.CONTENT_DISPOSITION, "attachment;filename="+ payload.getFileName());
        OutputStream out = response.getOutputStream();
        out.write(Base64.getDecoder().decode(payload.getBase64FileString()));
    }

    @GetMapping("/profile/file")
    public ResponseEntity<String> downloadFile(
            @RequestHeader(name = "user_id") String userId,
            HttpServletResponse response) throws IOException {
        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> res = userFileDownloadUseCase.downloadFile(userId);
        UserFilePayloadDTO data = res.getBody().getData();
        streamDownloadedFile(response, data);
        return ResponseEntity.ok().body("Success");
    }

    @GetMapping("/user/{userId}/file")
    public ResponseEntity<String> downloadFileById(
            @PathVariable String userId,
            HttpServletResponse response) throws IOException {
        ResponseEntity<ResponseDTO<UserFilePayloadDTO>> res = userFileDownloadUseCase.downloadFile(userId);
        UserFilePayloadDTO data = res.getBody().getData();
        streamDownloadedFile(response, data);
        return ResponseEntity.ok().body("Success");
    }
}
