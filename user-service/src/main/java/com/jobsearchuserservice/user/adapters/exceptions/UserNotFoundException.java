package com.jobsearchuserservice.user.adapters.exceptions;

import com.jobsearchuserservice.user.adapters.exceptions.commons.DataNotExistException;

public class UserNotFoundException extends DataNotExistException {
    public UserNotFoundException() {
        super("Referred user doesn't exist");
    }
}
