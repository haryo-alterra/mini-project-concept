package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.PaginationUserEntityDTO;
import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.application.in.UserProfileRetrieveAllUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserProfileAllRetrievalController {
    private final UserProfileRetrieveAllUseCase userProfileRetrieveAllUseCase;

    @GetMapping
    public ResponseEntity<ResponseDTO<PaginationUserEntityDTO>> retrieveAllProfiles(
            @PageableDefault Pageable pageable) {
        PaginationUserEntityDTO entityDTO = userProfileRetrieveAllUseCase.getAllUsers(pageable);
        return new ResponseEntity(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully retrieved all user profiles")
                .data(entityDTO).build(), HttpStatus.OK
        );
    }
}
