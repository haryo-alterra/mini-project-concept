package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileDeleteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserFileDeleteController {
    private final UserFileDeleteUseCase userFileDeleteUseCase;

    @DeleteMapping("/profile/file")
    public ResponseEntity<ResponseDTO<UserFileDTO>> deleteFile(@RequestHeader(name = "user_id") String userId) {
        return userFileDeleteUseCase.deleteFile(userId);
    }
}
