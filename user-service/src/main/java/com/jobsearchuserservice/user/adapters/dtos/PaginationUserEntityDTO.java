package com.jobsearchuserservice.user.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class PaginationUserEntityDTO implements Serializable {
    @JsonProperty(value = "job_applications_list")
    private List<UserEntityDTO> userDTOList;

    @JsonProperty(value = "total_of_items")
    private Long totalOfItems;

    @JsonProperty(value = "current_page")
    private Integer currentPage;

    @JsonProperty(value = "total_of_pages")
    private Integer totalOfPages;

}
