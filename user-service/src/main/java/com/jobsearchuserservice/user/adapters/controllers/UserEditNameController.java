package com.jobsearchuserservice.user.adapters.controllers;

import com.jobsearchuserservice.user.adapters.dtos.NameChangeDTO;
import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.application.in.UserEditNameUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/api/v1/profile")
@RequiredArgsConstructor
public class UserEditNameController {
    private final KafkaTemplate<String, Object> template;

    private final UserEditNameUseCase userEditNameUseCase;

    @Value("${tpc.topic-name}")
    private String topicName;

    @PutMapping
    public ResponseEntity<ResponseDTO<UserEntityDTO>> editName(
            @RequestHeader(name = "user_id") String userId,
            @RequestBody NameChangeDTO newName) {
        UserEntityDTO newUser = userEditNameUseCase.editName(userId, newName);
        HashMap<String, String> packagedData = new HashMap<>();
        packagedData.put("user_id", newUser.getId().toString());
        packagedData.put("name", newUser.getName());
        packagedData.put(
                "status", "Update name for user with ID " + newUser.getId());
        template.send(topicName, String.valueOf(1), packagedData);
        return new ResponseEntity(ResponseDTO.builder()
                .httpStatus(HttpStatus.OK)
                .message("Successfully updated your name")
                .data(newUser).build(), HttpStatus.OK
        );
    }
}


