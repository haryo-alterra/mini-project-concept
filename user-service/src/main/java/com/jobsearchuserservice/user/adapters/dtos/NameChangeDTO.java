package com.jobsearchuserservice.user.adapters.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class NameChangeDTO {
    @JsonProperty(required = true)
    private String name;
}
