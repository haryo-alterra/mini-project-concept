package com.jobsearchuserservice.user.utils;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

public class FileRequestGeneration {
    public static MultiValueMap<String, String> generate(
        String userId, MultipartFile file) throws IOException {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("user_id", userId);
        body.add("file_name", file.getOriginalFilename());
        body.add("file", Base64.getEncoder().encodeToString(file.getBytes()));
        return body;
    }
}
