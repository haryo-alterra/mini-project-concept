package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import com.jobsearchuserservice.user.application.in.UserFileDownloadUseCase;
import com.jobsearchuserservice.user.application.out.UserFileDownloadPort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileDownloadService implements UserFileDownloadUseCase {
    private final UserFileDownloadPort userFileDownloadPort;

    @Override
    public ResponseEntity<ResponseDTO<UserFilePayloadDTO>> downloadFile(String userId) {
        return userFileDownloadPort.downloadFile(userId);
    }
}
