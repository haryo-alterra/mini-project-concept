package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.PaginationUserEntityDTO;
import org.springframework.data.domain.Pageable;

public interface UserProfileRetrieveAllPort {
    PaginationUserEntityDTO getAllUsers(Pageable pageable);
}
