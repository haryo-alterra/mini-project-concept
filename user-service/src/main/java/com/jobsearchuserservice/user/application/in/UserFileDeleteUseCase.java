package com.jobsearchuserservice.user.application.in;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import org.springframework.http.ResponseEntity;

public interface UserFileDeleteUseCase {
    ResponseEntity<ResponseDTO<UserFileDTO>> deleteFile(String userId);
}
