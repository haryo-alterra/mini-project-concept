package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import org.springframework.http.ResponseEntity;

public interface UserFileDeletePort {
    ResponseEntity<ResponseDTO<UserFileDTO>> deleteFile(String userId);
}
