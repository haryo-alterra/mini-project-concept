package com.jobsearchuserservice.user.application.out;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserFileUploadPort {
    ResponseEntity<String> uploadFile(String userId, MultipartFile file) throws IOException;
}
