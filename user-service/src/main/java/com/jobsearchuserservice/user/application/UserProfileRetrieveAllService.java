package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.PaginationUserEntityDTO;
import com.jobsearchuserservice.user.application.in.UserProfileRetrieveAllUseCase;
import com.jobsearchuserservice.user.application.out.UserProfileRetrieveAllPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserProfileRetrieveAllService implements UserProfileRetrieveAllUseCase {
    private final UserProfileRetrieveAllPort userProfileRetrieveAllPort;

    @Override
    public PaginationUserEntityDTO getAllUsers(Pageable pageable) {
        return userProfileRetrieveAllPort.getAllUsers(pageable);
    }

}
