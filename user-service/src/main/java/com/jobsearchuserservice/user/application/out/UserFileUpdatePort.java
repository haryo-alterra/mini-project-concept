package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserFileUpdatePort {
    ResponseEntity<ResponseDTO<UserFileDTO>> updateFile(String userId, MultipartFile file) throws IOException;
}
