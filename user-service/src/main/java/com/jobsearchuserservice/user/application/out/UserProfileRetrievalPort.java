package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;

public interface UserProfileRetrievalPort {
    UserEntityDTO getUserProfileById(String userId);
}
