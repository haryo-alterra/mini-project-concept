package com.jobsearchuserservice.user.application.in;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserFileUploadUseCase {
    ResponseEntity<String> uploadFile(String userId, MultipartFile file) throws IOException;
}
