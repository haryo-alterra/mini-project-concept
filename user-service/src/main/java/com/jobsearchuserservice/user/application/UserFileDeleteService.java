package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileDeleteUseCase;
import com.jobsearchuserservice.user.application.out.UserFileDeletePort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserFileDeleteService implements UserFileDeleteUseCase {
    private final UserFileDeletePort userFileDeletePort;

    @Override
    public ResponseEntity<ResponseDTO<UserFileDTO>> deleteFile(String userId) {
        return userFileDeletePort.deleteFile(userId);
    }
}
