package com.jobsearchuserservice.user.application.in;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import org.springframework.http.ResponseEntity;

public interface UserFileDownloadUseCase {
    ResponseEntity<ResponseDTO<UserFilePayloadDTO>> downloadFile(String userId);
}
