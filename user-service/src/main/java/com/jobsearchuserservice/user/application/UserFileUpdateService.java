package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFileDTO;
import com.jobsearchuserservice.user.application.in.UserFileUpdateUseCase;
import com.jobsearchuserservice.user.application.out.UserFileUpdatePort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class UserFileUpdateService implements UserFileUpdateUseCase {
    private final UserFileUpdatePort userFileUpdatePort;

    @Override
    public ResponseEntity<ResponseDTO<UserFileDTO>> updateFile(String userId, MultipartFile file)
            throws IOException {
        return userFileUpdatePort.updateFile(userId, file);
    }
}
