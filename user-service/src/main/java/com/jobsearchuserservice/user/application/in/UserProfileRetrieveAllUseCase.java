package com.jobsearchuserservice.user.application.in;

import com.jobsearchuserservice.user.adapters.dtos.PaginationUserEntityDTO;
import org.springframework.data.domain.Pageable;

public interface UserProfileRetrieveAllUseCase {
    PaginationUserEntityDTO getAllUsers(Pageable pageable);
}
