package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.NameChangeDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;

public interface UserEditNamePort {
    UserEntityDTO editName(String userId, NameChangeDTO newName);
}
