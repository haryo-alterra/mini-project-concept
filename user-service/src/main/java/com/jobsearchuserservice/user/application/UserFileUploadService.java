package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.application.in.UserFileUploadUseCase;
import com.jobsearchuserservice.user.application.out.UserFileUploadPort;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
public class UserFileUploadService implements UserFileUploadUseCase {
    private final UserFileUploadPort userFileUploadPort;

    @Override
    public ResponseEntity<String> uploadFile(String userId, MultipartFile file) throws IOException{
        return userFileUploadPort.uploadFile(userId, file);
    }
}
