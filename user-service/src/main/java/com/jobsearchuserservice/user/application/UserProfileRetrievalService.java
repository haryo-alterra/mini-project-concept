package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.application.in.UserProfileRetrievalUseCase;
import com.jobsearchuserservice.user.application.out.UserProfileRetrievalPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserProfileRetrievalService implements UserProfileRetrievalUseCase {
    private final UserProfileRetrievalPort userProfileRetrievalPort;

    @Override
    public UserEntityDTO getUserProfileById(String userId) {
        return userProfileRetrievalPort.getUserProfileById(userId);
    }
}
