package com.jobsearchuserservice.user.application.in;

import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;

public interface UserProfileRetrievalUseCase {
    UserEntityDTO getUserProfileById(String userId);
}
