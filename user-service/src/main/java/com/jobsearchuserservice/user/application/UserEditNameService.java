package com.jobsearchuserservice.user.application;

import com.jobsearchuserservice.user.adapters.dtos.NameChangeDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;
import com.jobsearchuserservice.user.application.in.UserEditNameUseCase;
import com.jobsearchuserservice.user.application.out.UserEditNamePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserEditNameService implements UserEditNameUseCase {
    private final UserEditNamePort userEditNamePort;

    @Override
    public UserEntityDTO editName(String userId, NameChangeDTO newName) {
        return userEditNamePort.editName(userId, newName);
    }
}
