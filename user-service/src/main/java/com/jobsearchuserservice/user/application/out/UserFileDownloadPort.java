package com.jobsearchuserservice.user.application.out;

import com.jobsearchuserservice.user.adapters.dtos.ResponseDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserFilePayloadDTO;
import org.springframework.http.ResponseEntity;

public interface UserFileDownloadPort {
    ResponseEntity<ResponseDTO<UserFilePayloadDTO>> downloadFile(String userId);
}
