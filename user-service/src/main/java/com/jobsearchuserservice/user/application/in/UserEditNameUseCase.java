package com.jobsearchuserservice.user.application.in;

import com.jobsearchuserservice.user.adapters.dtos.NameChangeDTO;
import com.jobsearchuserservice.user.adapters.dtos.UserEntityDTO;

public interface UserEditNameUseCase {
    UserEntityDTO editName(String userId, NameChangeDTO newName);
}
