# Job Portal

A job portal app prototype. Project explanation can be accessed from a link below:

https://docs.google.com/presentation/d/1tF36zu8BPB2UxMSB9Hou0Nzio3X--H0zDFNTycs4Khc

To check available APIs, simply run

`docker compose up`

and go to http://localhost:6060/swagger-ui/index.html via browser