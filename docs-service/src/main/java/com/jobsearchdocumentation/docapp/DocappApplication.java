package com.jobsearchdocumentation.docapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableScheduling
@EnableWebMvc
@SpringBootApplication
public class DocappApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocappApplication.class, args);
	}

}
